//
// Created by Akos Polster on 11/04/2022.
// Copyright (C) 2022 Bragi. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    var window: UIWindow?

    typealias TabItem = (title: String, icon: String, vc: UIViewController)
    let tabItems: [TabItem] = [
        ("Devices", "headphones", DeviceListVC()),
        ("Settings", "gear", SettingsVC())
    ]

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        let window = UIWindow(windowScene: windowScene)
        let tabBar = UITabBarController()
        tabBar.viewControllers = tabItems.map { item in
            item.vc.tabBarItem = UITabBarItem(title: item.title, image: UIImage(systemName: item.icon), selectedImage: nil)
            item.vc.navigationItem.title = item.title
            return UINavigationController(rootViewController: item.vc)
        }
        window.rootViewController = tabBar
        window.makeKeyAndVisible()
        self.window = window
    }
}
