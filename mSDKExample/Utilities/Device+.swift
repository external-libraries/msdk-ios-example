//
// Created by Akos Polster on 29/04/2022.
// Copyright (C) 2022 Bragi. All rights reserved.
//

import Foundation
import mSDK

extension Device {
    /// Friendly name
    var shortName: String { name ?? "Unknown" }

    /// Long friendly name
    var longName: String {
        var name = shortName
        if let manufacturer = manufacturer {
            name += " (\(manufacturer))"
        } else if let manufacturerDescription = deviceConfig?.manufacturerDescription {
            name += " (\(manufacturerDescription))"
        }
        return name
    }
}
