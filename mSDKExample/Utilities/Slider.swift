//
// Created by Akos Polster on 18/05/2022
// Copyright (C) 2022 Bragi. All rights reserved.
//

import UIKit

/// UISlider with a title label
class Slider: UIView {
    private let valueChangeHandler: ((Float) -> Void)
    private let label = UILabel()
    private let slider = UISlider()

    init(title: String?, minimumValue: Float = 0, maximumValue: Float = 100, valueChangeHandler: @escaping ((Float) -> Void)) {
        self.valueChangeHandler = valueChangeHandler
        super.init(frame: .zero)
        addSubviewsForAutolayout(label, slider)
        label.text = title
        slider.addTarget(self, action: #selector(handleValueChange), for: .valueChanged)
        slider.minimumValue = minimumValue
        slider.maximumValue = maximumValue
        slider.isContinuous = false
        NSLayoutConstraint.activate([
            slider.topAnchor.constraint(equalTo: topAnchor),
            slider.trailingAnchor.constraint(equalTo: trailingAnchor),
            slider.widthAnchor.constraint(equalToConstant: 180),
            label.leadingAnchor.constraint(equalTo: leadingAnchor),
            label.trailingAnchor.constraint(equalTo: slider.leadingAnchor),
            label.centerYAnchor.constraint(equalTo: slider.centerYAnchor),
            bottomAnchor.constraint(equalTo: slider.bottomAnchor)
        ])
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    var value: Float {
        get { slider.value }
        set { slider.value = newValue }
    }

    var title: String? {
        get { label.text }
        set { label.text = newValue }
    }

    func setValue(_ value: Float, animated: Bool) {
        slider.setValue(value, animated: animated)
    }

    @objc private func handleValueChange() {
        valueChangeHandler(slider.value)
    }
}
