//
// Created by Akos Polster on 07/06/2022
// Copyright (C) 2022 Bragi. All rights reserved.
//

import UIKit

extension UIAlertController {
    /// Create a simple alert with a cancel button
    static func makeSimple(title: String? = nil, message: String? = nil, cancelTitle: String = "Close") -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: cancelTitle, style: .cancel))
        return alert
    }

    /// Create a progress HUD
    static func makeProgressHUD(title: String? = nil, message: String? = nil) -> UIAlertController {
        let alert = UIAlertController(title: (title ?? "") + "\n\n\n\n\n", message: message, preferredStyle: .alert)
        let indicator = UIActivityIndicatorView(frame: alert.view.bounds)
        indicator.style = .large
        indicator.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        alert.view.addSubview(indicator)
        indicator.isUserInteractionEnabled = false
        indicator.startAnimating()
        return alert
    }
}
