//
// Created by Akos Polster on 29/04/2022.
// Copyright (C) 2022 Bragi. All rights reserved.
//

import UIKit

extension UIStackView {
    /// Remove all arranged subviews
    @discardableResult
    func removeAllArrangedSubviews() -> [UIView] {
        return arrangedSubviews.reduce([UIView]()) { $0 + [removeArrangedSubViewProperly($1)] }
    }

    @discardableResult
    func removeArrangedSubViewProperly(_ view: UIView) -> UIView {
        removeArrangedSubview(view)
        view.removeFromSuperview()
        return view
    }
}
