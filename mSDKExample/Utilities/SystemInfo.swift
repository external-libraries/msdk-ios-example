//
// Created by Akos Polster on 10/01/2018.
// Copyright (C) 2022 Bragi. All rights reserved.
//

import Foundation
import UIKit

/// System (device and app) information
public struct SystemInfo {
    /// Shared instance
    public static var shared = SystemInfo()

    /// Version string
    public let version = (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String) ?? "0.0"

    /// Build number string
    public let build = (Bundle.main.infoDictionary?["CFBundleVersion"] as? String) ?? "0"

    /// Full version string: version.build
    public var fullVersion: String {
        let commitInfo: String
        if let hash = lastCommitShortHash, !hash.isEmpty {
            commitInfo = " \(hash)"
        } else {
            commitInfo = ""
        }
        return version + " (" + build + ")" + commitInfo
    }

    /// Version number
    public var versionNumber: Double { return Double(version) ?? 0 }

    #if DEBUG
        /// Is this a debug build?
        public let isDebugBuild = true
    #else
        /// Is this a debug build?
        public let isDebugBuild = false
    #endif

    /// Is this a test (Debug or TestFlight) build?
    public let isTestBuild: Bool = {
        #if DEBUG
            return true
        #else
            guard let receiptUrl = Bundle.main.appStoreReceiptURL else { return false }
            return receiptUrl.path.contains("sandboxReceipt")
        #endif
    }()

    /// Friendly operating system name
    public var osName: String {
        #if os(macOS)
        return "macOS"
        #elseif os(watchOS)
        return "watchOS"
        #else
        return UIDevice.current.systemName
        #endif
    }

    /// Friendly operating system version
    public var osVersion: String {
        #if os(macOS) || os(watchOS)
        let version = ProcessInfo.processInfo.operatingSystemVersion
        return String(format: "%d.%d.%d", version.majorVersion, version.minorVersion, version.patchVersion)
        #else
        return UIDevice.current.systemVersion
        #endif
    }

    /// Device identifier
    public var deviceId: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else {
                return identifier
            }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        return identifier
    }

    /// Friendly device name, like "iPhone 6s"
    public var deviceName: String {
        let identifier = deviceId
        switch identifier {
        case "iPod1,1":                                  return "iPod Touch"
        case "iPod2,1":                                  return "iPod Touch (2nd Gen)"
        case "iPod3,1":                                  return "iPod Touch (3rd Gen)"
        case "iPod4,1":                                  return "iPod Touch (4th Gen)"
        case "iPod5,1":                                  return "iPod Touch (5th Gen)"
        case "iPod7,1":                                  return "iPod Touch (6th Gen)"
        case "iPod9,1":                                  return "iPod Touch (7th Gen)"

        case "iPhone3,1", "iPhone3,2", "iPhone3,3":      return "iPhone 4"
        case "iPhone4,1":                                return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                   return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                   return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                   return "iPhone 5s"
        case "iPhone7,2":                                return "iPhone 6"
        case "iPhone7,1":                                return "iPhone 6 Plus"
        case "iPhone8,1":                                return "iPhone 6s"
        case "iPhone8,2":                                return "iPhone 6s Plus"
        case "iPhone9,1", "iPhone9,3":                   return "iPhone 7"
        case "iPhone9,2", "iPhone9,4":                   return "iPhone 7 Plus"
        case "iPhone8,4":                                return "iPhone SE"
        case "iPhone10,1", "iPhone10,4":                 return "iPhone 8"
        case "iPhone10,2", "iPhone10,5":                 return "iPhone 8 Plus"
        case "iPhone10,3", "iPhone10,6":                 return "iPhone X"
        case "iPhone11,2":                               return "iPhone Xs"
        case "iPhone11,4":                               return "iPhone Xs Max"
        case "iPhone11,8":                               return "iPhone XR"
        case "iPhone12,1":                               return "iPhone 11"
        case "iPhone12,3":                               return "iPhone 11 Pro"
        case "iPhone12,5":                               return "iPhone 11 Pro Max"
        case "iPhone12,8":                               return "iPhone SE (2nd Gen)"
        case "iPhone13,1":                               return "iPhone 12 Mini"
        case "iPhone13,2":                               return "iPhone 12"
        case "iPhone13,3":                               return "iPhone 12 Pro"
        case "iPhone13,4":                               return "iPhone 12 Pro Max"

        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4": return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":            return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":            return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":            return "iPad Air"
        case "iPad5,3", "iPad5,4":                       return "iPad Air 2"
        case "iPad2,5", "iPad2,6", "iPad2,7":            return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":            return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":            return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":                       return "iPad Mini 4"
        case "iPad6,3", "iPad6,4", "iPad6,7", "iPad6,8": return "iPad Pro"
        case "iPad7,1", "iPad7,2":                       return "iPad Pro 12.9\" 2nd Gen"
        case "iPad7,3", "iPad7,4":                       return "iPad Pro 10.5\""
        case "iPad7,5", "iPad7,6":                       return "iPad 6th Gen"
        case "iPad8,1", "iPad8.2", "iPad8,3", "iPad8,4": return "iPad Pro 11\""
        case "iPad8,5", "iPad8,6", "iPad8,7", "iPad8,8": return "iPad Pro 12.9\" (3rd Gen)"
        case "iPad8,11", "iPad8,12":                     return "iPad Pro 12.9\" (4th Gen)"
        case "iPad11,1", "iPad11,2":                     return "iPad Mini (5th Gen)"
        case "iPad11,3", "iPad11,4":                     return "iPad Air 3rd Gen"
        case "iPad11,6":                                 return "iPad 8th Gen (WiFi)"
        case "iPad11,7":                                 return "iPad 8th Gen (WiFi+Cellular)"
        case "iPad13,1":                                 return "iPad Air 4th Gen (WiFi)"
        case "iPad13,2":                                 return "iPad Air 4th Gen (WiFi+Celular)"

        case "AppleTV1,1":                               return "Apple TV"
        case "AppleTV2,1":                               return "Apple TV (2nd Gen)"
        case "AppleTV3,1", "AppleTV3,2":                 return "Apple TV (3rd Gen)"
        case "AppleTV5,3":                               return "Apple TV (4th Gen)"
        case "AppleTV6,2":                               return "Apple TV 4K"

        case "AudioAccessory1,1", "AudioAccessory1,2":   return "HomePod"
        case "AudioAccessory5,1":                        return "HomePod Mini"

        case "i386", "x86_64", "arm64":                  return "Simulator"

        default:                                         return identifier
        }
    }

    #if targetEnvironment(simulator)
        /// Are we running on the simulator?
        public var isSimulator = true
    #else
        /// Are we running on the simulator?
        public var isSimulator = false
    #endif

    /// The name of the app
    public let appName = (Bundle.main.infoDictionary!["CFBundleDisplayName"] as? String) ?? "App"

    /// Short hash of the last git commit to HEAD
    public let lastCommitShortHash = Bundle.main.infoDictionary!["LastCommitShort"] as? String
}
