//
// Created by Akos Polster on 02/06/2022
// Copyright (C) 2022 Bragi. All rights reserved.
//

import UIKit

/// View for displaying an error text
class ErrorView: UIView {
    var text: String? {
        get { label.text }
        set { label.text = newValue }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubviewsForAutolayout(label)
        label.anchorEdges(to: self, insets: UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15))
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private let label: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textColor = .systemRed
        return label
    }()
}
