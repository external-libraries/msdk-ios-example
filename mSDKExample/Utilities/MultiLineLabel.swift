//
// Created by Akos Polster on 15/06/2022
// Copyright (C) 2022 Bragi. All rights reserved.
//

import UIKit

/// Label with multiple lines
class MultiLineLabel: UILabel {
    init(text: String? = nil) {
        super.init(frame: .zero)
        self.numberOfLines = 0
        self.text = text
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.numberOfLines = 0
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
