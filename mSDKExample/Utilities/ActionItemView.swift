//
// Created by Tetiana Chunikhina on 03.06.2022
// Copyright (C) 2022 Bragi. All rights reserved.
//

import UIKit

class ActionItemView: UIView {
    private let label = UILabel()
    private let image = UIImageView()
    private let separator = UIView()

    var onTap: (() -> Void)?

    init(item: ActionItem) {
        super.init(frame: .zero)
        let tap = UITapGestureRecognizer(target: self, action: #selector(tap))
        addGestureRecognizer(tap)
        addSubviewsForAutolayout(label, image, separator)
        label.text = item.title
        setSelected(item.isSelected)
        separator.backgroundColor = UIColor.gray
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            image.heightAnchor.constraint(equalToConstant: 20),
            image.widthAnchor.constraint(equalToConstant: 20),
            image.trailingAnchor.constraint(equalTo: trailingAnchor),
            label.leadingAnchor.constraint(equalTo: leadingAnchor),
            label.trailingAnchor.constraint(greaterThanOrEqualTo: image.leadingAnchor, constant: 10),
            label.centerYAnchor.constraint(equalTo: image.centerYAnchor),
            separator.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 1),
            separator.leadingAnchor.constraint(equalTo: leadingAnchor),
            separator.trailingAnchor.constraint(equalTo: trailingAnchor),
            separator.heightAnchor.constraint(equalToConstant: 1),
            bottomAnchor.constraint(equalTo: label.bottomAnchor, constant: 10)
        ])
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setSelected(_ isSelected: Bool) {
        image.image = isSelected ? UIImage.checkmark : nil
    }

    @objc func tap() {
        onTap?()
    }
}
