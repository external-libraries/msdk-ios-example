//
// Created by Tetiana Chunikhina on 03.06.2022
// Copyright (C) 2022 Bragi. All rights reserved.
//

import Foundation
import mSDKCore

struct ActionItem {
    var option: ActionButtonType
    var title: String?
    var isSelected: Bool
}

enum ActionButtonType: Int {
    case activeOff = 1
    case telephonyOff = -1
    case toggleAncTransparency = 0
    case volumeUp = 2
    case volumeDown = 3
    case playPause = 4
    case nextTrack = 5
    case previousTrack = 6
    case takeCall = 7
    case rejectCall = 8
    case endCall = 9
    case takeEndCall = 10
    case rejectEndCall = 11
    case error = 12
    case fastMuteToggle = 13
    case remoteUp = 14
    case voiceAssistant = 15
    case spotifyOneTouch = 16
    case remoteSelfie = 17

    var title: String? {
        switch self {
        case .activeOff:
            return "activeOff"
        case .telephonyOff:
            return "telephonyOff"
        case .toggleAncTransparency:
            return "toggleAncTransparency"
        case .playPause:
            return "playPause"
        case .nextTrack:
            return "nextTrack"
        case .previousTrack:
            return "previousTrack"
        case .volumeUp:
            return "volumeUp"
        case .volumeDown:
            return "volumeDown"
        case .remoteSelfie:
            return "remoteSelfie"
        case .voiceAssistant:
            return "voiceAssistant"
        case .takeCall:
            return "takeCall"
        case .rejectEndCall:
            return "rejectEndCall"
        case .takeEndCall:
            return "takeEndCall"
        case .spotifyOneTouch:
            return "spotifyOneTouch"
        default:
            return nil
        }
    }
}

extension ActionButtonType {
    // swiftlint: disable cyclomatic_complexity
    static func make(_ event: UI.Command) -> ActionButtonType? {
        switch event {
        case .remoteUp:
            return .remoteUp
        case .telephonyTake:
            return .takeCall
        case .telephonyReject:
            return .rejectCall
        case .telephonyEnd:
            return .endCall
        case .telephonyRejectEnd:
            return .rejectEndCall
        case .telephonyTakeEnd:
            return .takeEndCall
        case .mediaPlay:
            return .playPause
        case .mediaNext:
            return .nextTrack
        case .mediaPrevious:
            return .previousTrack
        case .mediaPlayToogle:
            return .playPause
        case .fastMuteToogle:
            return .fastMuteToggle
        case .ancToggle, .ancAncToogle, .ancATToggle:
            return .toggleAncTransparency
        case .volumeUp:
            return .volumeUp
        case .volumeDown:
            return .volumeDown
        case .assistantToggle:
            return .voiceAssistant
        case .spotifyOnetouch:
            return .spotifyOneTouch
        case .remoteHidVolumeUp:
            return .remoteSelfie
        default:
            return nil
        }
    }

    var command: UI.Command? {
        switch self {
        case .error, .telephonyOff, .activeOff:
            return nil
        case .volumeUp:
            return .volumeUp
        case .volumeDown:
            return .volumeDown
        case .playPause:
            return .mediaPlayToogle
        case .nextTrack:
            return .mediaNext
        case .previousTrack:
            return .mediaPrevious
        case .takeCall:
            return .telephonyTake
        case .rejectCall:
            return .telephonyReject
        case .endCall:
            return .telephonyEnd
        case .takeEndCall:
            return .telephonyTakeEnd
        case .rejectEndCall:
            return .telephonyRejectEnd
        case .fastMuteToggle:
            return .fastMuteToogle
        case .remoteUp:
            return .remoteUp
        case .voiceAssistant:
            return .assistantToggle
        case .spotifyOneTouch:
            return .spotifyOnetouch
        case .remoteSelfie:
            return .remoteHidVolumeUp
        case .toggleAncTransparency:
            return .ancToggle
        }
    }
}
