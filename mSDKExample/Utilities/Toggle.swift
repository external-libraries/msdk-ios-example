//
// Created by Akos Polster on 18/05/2022
// Copyright (C) 2022 Bragi. All rights reserved.
//

import UIKit

/// A UISwitch with a title label
class Toggle: UIView {
    private let valueChangeHandler: ((Bool) -> Void)
    private let label = UILabel()
    private let toggle = UISwitch()

    init(title: String?, valueChangeHandler: @escaping ((Bool) -> Void)) {
        self.valueChangeHandler = valueChangeHandler
        super.init(frame: .zero)
        addSubviewsForAutolayout(label, toggle)
        label.text = title
        toggle.addTarget(self, action: #selector(handleValueChange), for: .valueChanged)
        NSLayoutConstraint.activate([
            toggle.topAnchor.constraint(equalTo: topAnchor),
            toggle.trailingAnchor.constraint(equalTo: trailingAnchor),
            label.leadingAnchor.constraint(equalTo: leadingAnchor),
            label.trailingAnchor.constraint(equalTo: toggle.leadingAnchor),
            label.centerYAnchor.constraint(equalTo: toggle.centerYAnchor),
            bottomAnchor.constraint(equalTo: toggle.bottomAnchor)
        ])
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    var isOn: Bool {
        get { toggle.isOn }
        set { toggle.isOn = newValue }
    }

    func setOn(_ on: Bool, animated: Bool) {
        toggle.setOn(on, animated: animated)
    }

    var title: String? {
        get { label.text }
        set { label.text = newValue }
    }

    @objc private func handleValueChange() {
        valueChangeHandler(toggle.isOn)
    }
}
