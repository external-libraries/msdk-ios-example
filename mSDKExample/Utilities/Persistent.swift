//
// Created by Akos Polster on 03/12/2019.
// Copyright (C) 2022 Bragi. All rights reserved.
//

import Foundation

private let encoder = JSONEncoder()
private let decoder = JSONDecoder()

/// Wrapper for persistent properties stored in UserDefaults
///
/// Usage:
/// ```
/// @Persistent(key: "Settings.lastModified", defaultValue: Date.distantPast)
/// private var lastModified: Date
/// ```
@propertyWrapper
public struct Persistent<T: Codable> {
    public let key: String
    public let defaults: UserDefaults
    public let defaultValue: T

    private struct Box: Codable {
        let value: T?
    }

    public init(key: String, defaultValue: T, defaults: UserDefaults = UserDefaults.standard) {
        self.key = key
        self.defaultValue = defaultValue
        self.defaults = defaults
    }

    public var wrappedValue: T {
        get { return Self.getValue(key: key, defaultValue: defaultValue, defaults: defaults) }
        set { Self.setValue(newValue, key: key, defaults: defaults) }
    }

    public static func getValue(key: String, defaultValue: T, defaults: UserDefaults) -> T {
        guard let data = defaults.data(forKey: key) else { return defaultValue }
        do {
            let boxed = try decoder.decode(Box.self, from: data)
            return boxed.value ?? defaultValue
        } catch {
            return defaultValue
        }
    }

    public static func setValue(_ newValue: T, key: String, defaults: UserDefaults) {
        let boxed = Box(value: newValue)
        defaults.set(try? encoder.encode(boxed), forKey: key)
    }
}
