//
// Created by Akos Polster on 29/04/2022.
// Copyright (C) 2022 Bragi. All rights reserved.
//

import Foundation
import mSDK
import RxBluetoothKit
import CoreBluetooth

extension Bragi {
    /// The shared Bragi instance
    static let shared: Bragi = {
        let manager = CentralManager(
            queue: .main,
            options: [CBCentralManagerOptionShowPowerAlertKey: true as AnyObject],
            cbCentralManager: nil)
        return Bragi(manager)
    }()
}
