//
// Created by Akos Polster on 04/05/2022
// Copyright (C) 2022 Bragi. All rights reserved.
//

import UIKit

extension UIActivityIndicatorView {
    /// Show indicator over a view
    func show(over view: UIView) {
        hide()
        view.addSubviewsForAutolayout(self)
        NSLayoutConstraint.activate([
            centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor),
            centerYAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerYAnchor)
        ])
        view.isUserInteractionEnabled = false
        startAnimating()
    }

    /// Hide indicator
    func hide() {
        stopAnimating()
        superview?.isUserInteractionEnabled = true
        removeFromSuperview()
    }
}
