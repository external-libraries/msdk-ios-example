//
// Created by Akos Polster on 29/04/2022.
// Copyright (C) 2022 Bragi. All rights reserved.
//

import Foundation
import mSDK

extension Gender: CustomStringConvertible {
    public var description: String {
        switch self {
        case .left: return "left"
        case .right: return "right"
        case .unknown: return "unknown"
        @unknown default: return "unknown"
        }
    }
}
