//
// Created by Akos Polster on 04/05/2022
// Copyright (C) 2022 Bragi. All rights reserved.
//

/// Settings row model
class SettingsRow {
    let title: String
    let detail: String?
    let action: (() -> Void)?
    var isChecked: Bool

    init(title: String, detail: String? = nil, isChecked: Bool = false, action: (() -> Void)? = nil) {
        self.title = title
        self.detail = detail
        self.isChecked = isChecked
        self.action = action
    }
}

/// Settings section model
class SettingsSection {
    let title: String
    var rows: [SettingsRow]
    let footer: String?

    init(title: String, rows: [SettingsRow], footer: String? = nil) {
        self.title = title
        self.rows = rows
        self.footer = footer
    }
}
