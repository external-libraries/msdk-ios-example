//
// Created by Akos Polster on 11/04/2022.
// Copyright (C) 2022 Bragi. All rights reserved.
//

import UIKit
import mSDK

/// Settings UI
class SettingsVC: UITableViewController {
    init() {
        super.init(style: .insetGrouped)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .secondarySystemBackground
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        settings.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        settings[section].rows.count
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        settings[section].title
    }

    override func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        settings[section].footer
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let setting = settings[indexPath.section].rows[indexPath.row]
        let cell = UITableViewCell(style: .value1, reuseIdentifier: nil)
        cell.textLabel?.text = setting.title
        if #available(iOS 15.0, *), setting.action != nil { cell.textLabel?.textColor = .tintColor }
        cell.detailTextLabel?.text = setting.detail
        cell.isUserInteractionEnabled = setting.action != nil
        cell.accessoryType = setting.isChecked ? .checkmark : .none
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let action = settings[indexPath.section].rows[indexPath.row].action { action() }
    }

    // MARK: - Internal

    private lazy var settings: [SettingsSection] = [
        SettingsSection(
            title: "Versions",
            rows: [
                SettingsRow(title: "App", detail: SystemInfo.shared.appName + " " + SystemInfo.shared.fullVersion),
                SettingsRow(title: "mSDK", detail: Bragi.version),
                SettingsRow(title: SystemInfo.shared.osName, detail: SystemInfo.shared.osVersion)
            ]
        ),
        SettingsSection(
            title: "Logs",
            rows: [
                SettingsRow(title: "Export Logs", action: exportLogs)
            ]
        )
    ]
    private lazy var waitIndicator = UIActivityIndicatorView(style: .large)

    private func exportLogs() {
        waitIndicator.show(over: tableView)
        DispatchQueue.global().async {
            let recentLog = Logger.recentLog.joined(separator: "\n")
            DispatchQueue.main.async { [weak self] in
                self?.waitIndicator.hide()
                self?.present(UIActivityViewController(activityItems: [recentLog], applicationActivities: nil), animated: true)
            }
        }
    }
}
