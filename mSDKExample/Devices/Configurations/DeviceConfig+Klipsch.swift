//
//  DeviceConfig+Klipsch.swift
//  WLA_Redesign
//
//  Created by Brody Robertson on 4/14/22.
//  Copyright © 2022 Bragi. All rights reserved.
//

import mSDK

public extension DeviceConfig {

    /// All Klipsch supported DeviceConfigs
    /// - The order of provided DeviceConfigs is important - DeviceConfigs with more specific discovery semantics should be ordered first
    /// - Catch-all DeviceConfigs with only companyIdData discovery semantics should be ordered last
    static let klipsch: [DeviceConfig] = [.klipschT5ANC]

    // MARK: - Klipsch T5 II True Wireless ANC

    /// DeviceConfig for Klipsch T5 II True Wireless ANC
    static let klipschT5ANC = DeviceConfig(manufacturerDescription: "Klipsch Group, Inc.",
                                           modelDescription: "Klipsch T5 II True Wireless ANC",
                                           type: .inEarHeadphones,
                                           bleConfig: BLEConfig(companyIdData: Data([0xFA, 0x07]), modelIdDataRange: 2..<5, modelIdData: Data([0x10, 0x4F, 0xE6]), localName: nil),
                                           mfiConfig: nil,
                                           primaryConnectionServiceType: .ble,
                                           features: klipschT5ANCFeatures)

    /// Features for Klipsch T5 II True Wireless ANC
    static var klipschT5ANCFeatures: [Feature] {

        return [Feature(type: .session, serviceType: .airohaRACEBLE),
                Feature(type: .tws, serviceType: .airohaRACEBLE),
                Feature(type: .device, serviceType: .bosATBLEAiroha),
                Feature(type: .feature, serviceType: .bosATBLEAiroha),
                Feature(type: .utility, serviceType: .bosATBLEAiroha),
                Feature(type: .ancGain, serviceType: .airohaRACEBLE),
                Feature(type: .transparency, serviceType: .bosATBLEAiroha),
                Feature(type: .audio, serviceType: .bosATBLEAiroha),
                Feature(type: .equalizer, serviceType: .airohaRACEBLE),
                Feature(type: .ui, serviceType: .bosATBLEAiroha),
                Feature(type: .gesture, serviceType: .bosATBLEAiroha),
                Feature(type: .system, serviceType: .bosATBLEAiroha),
                Feature(type: .language, serviceType: .bosATBLEAiroha),
                Feature(type: .fota(externalConnectionManaged: true), serviceType: .airohaRACEBLE),
                // TODO: mSDK5 determine if voice should be unavailable for KlipschT5ANC
                Feature(type: .voice, serviceType: .bosATBLEAiroha),
                // Unavailable services can be omitted or specified as unavailable
                // Feature(type: .voiceStream, serviceType: .unavailable),
                Feature(type: .thirdParty, serviceType: .bosATBLEAiroha)]

    }

}
