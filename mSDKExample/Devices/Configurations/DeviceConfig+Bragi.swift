//
//  DeviceConfig+Bragi.swift
//  WLA_Redesign
//
//  Created by Brody Robertson on 5/6/22.
//  Copyright © 2022 Bragi. All rights reserved.
//

import mSDK

public extension DeviceConfig {

    /// All Bragi supported DeviceConfigs
    /// - The order of provided DeviceConfigs is important - DeviceConfigs with more specific discovery semantics should be ordered first
    /// - Catch-all DeviceConfigs with only companyIdData discovery semantics should be ordered last
    static let bragi: [DeviceConfig] = [.bragiWuQi7033AX, .bragi1562MFI, .bragi1562BLET80, .bragi1562BLE]

    // MARK: - Bragi WuQi 7033AX

    /// DeviceConfig for Bragi WuQi 7033AX
    static let bragiWuQi7033AX = DeviceConfig(manufacturerDescription: "BRAGI GmbH",
                                              modelDescription: "Bragi WuQi 7033AX",
                                              type: .inEarHeadphones,
                                              // bleConfig: BLEConfig(companyIdData: Data([0x41, 0x02])),
                                              bleConfig: BLEConfig(companyIdData: Data([0x41, 0x02]), modelIdDataRange: 2..<4, modelIdData: Data([0x0D, 0x00]), localName: nil),
                                              mfiConfig: nil,
                                              primaryConnectionServiceType: .ble,
                                              features: bragiWuQi7033AXFeatures)

    /// Features for Bragi WuQi 7033AX
    static var bragiWuQi7033AXFeatures: [Feature] {
        return [Feature(type: .session, serviceType: .wuqiBLE),
                Feature(type: .fota(externalConnectionManaged: false), serviceType: .wuqiBLE),
                Feature(type: .device, serviceType: .wuqiBLE)]

    }

    // MARK: - Bragi 1562 Boards

    /// DeviceConfig for 1562 board connected via EAP
    static let bragi1562MFI = DeviceConfig(
        manufacturerDescription: "Bragi Gmbh",
        modelDescription: "AB1562/MFi",
        type: .inEarHeadphones,
        bleConfig: nil,
        mfiConfig: MFIConfig(
            modelNumber: "BT_iAP2",
            data: nil,
            audio: nil,  // TODO: Use "com.skullcandy.audio" when firmware supports it
            race: "com.skullcandy.race"),
        primaryConnectionServiceType: .mfi,
        features: bragi1562MFIFeatures)

    /// DeviceConfig for 1562 board broadcasting T80 manufacturer data
    static let bragi1562BLET80 = DeviceConfig(
        manufacturerDescription: "Bragi Gmbh",
        modelDescription: "AB1562BLE/T80",
        type: .inEarHeadphones,
        bleConfig: BLEConfig(companyIdData: Data([0xC9, 0x07]), modelIdDataRange: 3..<4, modelIdData: Data([0x15]), localName: nil),
        mfiConfig: nil,
        primaryConnectionServiceType: .ble,
        features: bragi1562BLEFeatures)

    /// DeviceConfig for bare-bones 1562 board
    static let bragi1562BLE = DeviceConfig(
        manufacturerDescription: "Bragi Gmbh",
        modelDescription: "AB1562BLE",
        type: .inEarHeadphones,
        bleConfig: BLEConfig(companyIdData: nil, modelIdDataRange: nil, modelIdData: nil, localName: "AB1562BLE"),
        mfiConfig: nil,
        primaryConnectionServiceType: .ble,
        features: bragi1562BLEFeatures)

    /// Features for 1562 boards connected via BLE
    static var bragi1562BLEFeatures: [Feature] = [
        Feature(type: .session, serviceType: .airohaRACEBLE),
        Feature(type: .fota(externalConnectionManaged: true), serviceType: .airohaRACEBLE),
        // Feature(type: .device, serviceType: .airohaRACEBLE),
    ]

    /// Features for 1562 boards connected via EAP
    static var bragi1562MFIFeatures: [Feature] = [
        Feature(type: .session, serviceType: .airohaRACEMFI),
        Feature(type: .fota(externalConnectionManaged: true), serviceType: .airohaRACEMFI),
        Feature(type: .device, serviceType: .airohaRACEMFI),
    ]
}
