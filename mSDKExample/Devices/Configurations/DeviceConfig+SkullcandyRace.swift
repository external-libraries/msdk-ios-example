//
//  Created by Akos Polster on 02/05/2022.
//  Copyright © 2022 Bragi. All rights reserved.
//

import mSDK

public extension DeviceConfig {
    /// All Skullcandy-over-RACE DeviceConfigs
    static let skullcandyRACE: [DeviceConfig] = [.skullcandyGrindFuelRACE]

    /// DeviceConfig for Skullcandy Grind Fuel device managed using the RACE protocol
    static let skullcandyGrindFuelRACE = DeviceConfig(
        manufacturerDescription: "Skullcandy, Inc.",
        modelDescription: "Grind Fuel RACE",
        type: .inEarHeadphones,
        bleConfig: nil,
        mfiConfig: MFIConfig(modelNumber: "S2GFW-P740", data: "com.skullcandy.data", audio: "com.skullcandy.audio", race: "com.skullcandy.race"),
        primaryConnectionServiceType: .mfi,
        features: skullcandyGrindFuelRACEFeatures)

    /// Features for Skullcandy Grind Fuel over RACE
    static var skullcandyGrindFuelRACEFeatures: [Feature] {
        [
            Feature(type: .session, serviceType: .airohaRACEMFI),
            Feature(type: .tws, serviceType: .airohaRACEMFI),
            Feature(type: .device, serviceType: .airohaRACEMFI),
            Feature(type: .equalizer, serviceType: .airohaRACEMFI),
            Feature(type: .fota(externalConnectionManaged: true), serviceType: .airohaRACEMFI),
            Feature(type: .ancGain, serviceType: .airohaRACEMFI),
            Feature(type: .transparency, serviceType: .airohaRACEMFI),
            Feature(type: .audio, serviceType: .airohaRACEMFI),
            Feature(type: .system, serviceType: .airohaRACEMFI),
            Feature(type: .equalizer, serviceType: .airohaRACEMFI),
            Feature(type: .ui, serviceType: .airohaRACEMFI)
        ]
    }
}
