//
//  DeviceConfig+WLA.swift
//  WLA_Redesign
//
//  Created by Brody Robertson on 12/20/21.
//  Copyright © 2021 Bragi. All rights reserved.
//

import mSDK

public extension DeviceConfig {
    /// All mSDKExample supported DeviceConfigs
    static let all: [DeviceConfig] = bragi + klipsch + skullcandy + noise + _20DB + wuqi

    /// Grind Fuel via RACE, plus all supported DeviceConfigs
    static let grindFuelRACEPlusAll: [DeviceConfig] = skullcandyRACE + all
}
