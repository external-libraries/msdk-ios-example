//
//  DeviceConfig+Noise.swift
//  WLA_Redesign
//
//  Created by Brody Robertson on 4/14/22.
//  Copyright © 2022 Bragi. All rights reserved.
//

import mSDK

public extension DeviceConfig {

    /// All Noise supported DeviceConfigs
    /// - The order of provided DeviceConfigs is important - DeviceConfigs with more specific discovery semantics should be ordered first
    /// - Catch-all DeviceConfigs with only companyIdData discovery semantics should be ordered last
    static let noise: [DeviceConfig] = []

    // FIXME: mSDK5 ADD Noise DeviceConfig

}
