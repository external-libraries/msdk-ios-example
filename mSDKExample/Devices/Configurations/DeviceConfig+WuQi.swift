//
// Created by Brody Robertson on 6/3/22
// Copyright (C) 2022 Bragi. All rights reserved.
//

import mSDK

public extension DeviceConfig {

    /// All WuQi supported DeviceConfigs
    /// - The order of provided DeviceConfigs is important - DeviceConfigs with more specific discovery semantics should be ordered first
    /// - Catch-all DeviceConfigs with only companyIdData discovery semantics should be ordered last
    static let wuqi: [DeviceConfig] = [.wuqi7033AX]

    // MARK: - WuQi 7033AX

    /// DeviceConfig for WuQi 7033AX
    static let wuqi7033AX = DeviceConfig(manufacturerDescription: "WuQi",
                                         modelDescription: "WuQi 7033AX",
                                         type: .inEarHeadphones,
                                         bleConfig: BLEConfig(advertisedServiceUUIDs: [Data([0xAA, 0xAA, 0xAA, 0xAA])]),
                                         mfiConfig: nil,
                                         primaryConnectionServiceType: .ble,
                                         features: wuqi7033AXFeatures)

    /// Features for WuQi 7033AX
    static var wuqi7033AXFeatures: [Feature] {
        return [Feature(type: .session, serviceType: .wuqiBLE),
                Feature(type: .fota(externalConnectionManaged: false), serviceType: .wuqiBLE),
                Feature(type: .device, serviceType: .wuqiBLE)]
    }

}
