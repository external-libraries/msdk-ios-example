//
//  DeviceConfig+Skullcandy.swift
//  WLA_Redesign
//
//  Created by Brody Robertson on 4/14/22.
//  Copyright © 2022 Bragi. All rights reserved.
//

import mSDK

public extension DeviceConfig {

    /// All Skullcandy supported DeviceConfigs
    /// - The order of provided DeviceConfigs is important - DeviceConfigs with more specific discovery semantics should be ordered first
    /// - Catch-all DeviceConfigs with only companyIdData discovery semantics should be ordered last
    static let skullcandy: [DeviceConfig] = [.skullcandyPushActive, .skullcandyGrindFuel, .skullcandyCHP200, .anySkullcandyMFIDevice, .anySkullcandyBLEDevice]

    // MARK: - Skullcandy Push Active

    /// DeviceConfig for Skullcandy Push Active device
    static let skullcandyPushActive = DeviceConfig(manufacturerDescription: "Skullcandy, Inc.",
                                                   modelDescription: "Push Active",
                                                   type: .inEarHeadphones,
                                                   bleConfig: nil,
                                                   mfiConfig: MFIConfig(modelNumber: "S2BPW-P740", data: "com.skullcandy.data", audio: "com.skullcandy.audio", race: "com.skullcandy.race"),
                                                   primaryConnectionServiceType: .mfi,
                                                   features: skullcandyPushActiveFeatures)

    /// Features for Skullcandy Push Active device
    static var skullcandyPushActiveFeatures: [Feature] {

        return [Feature(type: .session, serviceType: .airohaRACEMFI),
                Feature(type: .tws, serviceType: .airohaRACEMFI),
                Feature(type: .device, serviceType: .bosATMFIAiroha),
                Feature(type: .feature, serviceType: .bosATMFIAiroha),
                Feature(type: .utility, serviceType: .bosATMFIAiroha),
                Feature(type: .ancGain, serviceType: .airohaRACEMFI),
                Feature(type: .transparency, serviceType: .bosATMFIAiroha),
                Feature(type: .audio, serviceType: .bosATMFIAiroha),
                Feature(type: .equalizer, serviceType: .airohaRACEMFI),
                Feature(type: .ui, serviceType: .bosATMFIAiroha),
                Feature(type: .gesture, serviceType: .bosATMFIAiroha),
                Feature(type: .system, serviceType: .bosATMFIAiroha),
                Feature(type: .language, serviceType: .bosATMFIAiroha),
                Feature(type: .fota(externalConnectionManaged: true), serviceType: .airohaRACEMFI),
                Feature(type: .voice, serviceType: .bosATMFIAiroha),
                Feature(type: .voiceStream, serviceType: .mfiAiroha),
                Feature(type: .thirdParty, serviceType: .bosATMFIAiroha)]

    }

    // MARK: - Skullcandy Grind Fuel

    /// DeviceConfig for Skullcandy Grind Fuel device
    static let skullcandyGrindFuel = DeviceConfig(manufacturerDescription: "Skullcandy, Inc.",
                                                  modelDescription: "Grind Fuel",
                                                  type: .inEarHeadphones,
                                                  bleConfig: nil,
                                                  mfiConfig: MFIConfig(modelNumber: "S2GFW-P740", data: "com.skullcandy.data", audio: "com.skullcandy.audio", race: "com.skullcandy.race"),
                                                  primaryConnectionServiceType: .mfi,
                                                  features: skullcandyGrindFuelFeatures)

    /// Features for Skullcandy Grind Fuel device
    static var skullcandyGrindFuelFeatures: [Feature] {

        return [Feature(type: .session, serviceType: .airohaRACEMFI),
                Feature(type: .tws, serviceType: .airohaRACEMFI),
                Feature(type: .device, serviceType: .bosATMFIAiroha),
                Feature(type: .feature, serviceType: .bosATMFIAiroha),
                Feature(type: .utility, serviceType: .bosATMFIAiroha),
                Feature(type: .ancGain, serviceType: .airohaRACEMFI),
                Feature(type: .transparency, serviceType: .bosATMFIAiroha),
                Feature(type: .audio, serviceType: .bosATMFIAiroha),
                Feature(type: .equalizer, serviceType: .airohaRACEMFI),
                Feature(type: .ui, serviceType: .bosATMFIAiroha),
                Feature(type: .gesture, serviceType: .bosATMFIAiroha),
                Feature(type: .system, serviceType: .bosATMFIAiroha),
                Feature(type: .language, serviceType: .bosATMFIAiroha),
                Feature(type: .fota(externalConnectionManaged: true), serviceType: .airohaRACEMFI),
                Feature(type: .voice, serviceType: .bosATMFIAiroha),
                Feature(type: .voiceStream, serviceType: .mfiAiroha),
                Feature(type: .thirdParty, serviceType: .bosATMFIAiroha)]

    }

    // MARK: - Skullcandy CHP200

    /// DeviceConfig for Skullcandy CHP200 device
    /// TODO: modelName should be changed in future
    static let skullcandyCHP200 = DeviceConfig(
        manufacturerDescription: "Skullcandy, Inc.",
        modelDescription: "CHP200",
        type: .overEarHeadphones,
        bleConfig: nil,
        mfiConfig: MFIConfig(modelNumber: "00000000", data: "com.skullcandy.data", audio: "com.skullcandy.audio", race: "com.skullcandy.race"),
        primaryConnectionServiceType: .mfi,
        features: skullcandyCHP200Features)

    /// Features for Skullcandy CHP200
    static var skullcandyCHP200Features: [Feature] {

        return [Feature(type: .session, serviceType: .airohaRACEMFI),
                Feature(type: .tws, serviceType: .airohaRACEMFI),
                Feature(type: .device, serviceType: .bosATMFIAiroha),
                Feature(type: .ancGain, serviceType: .airohaRACEMFI),
                Feature(type: .equalizer, serviceType: .airohaRACEMFI),
                Feature(type: .fota(externalConnectionManaged: true), serviceType: .airohaRACEMFI)]

    }

    // MARK: - Any Skullcandy

    /// DeviceConfig for any Skullcandy device based on MFI limited MFI data
    static let anySkullcandyMFIDevice = DeviceConfig(manufacturerDescription: "Skullcandy, Inc.",
                                                     modelDescription: "Any Skullcandy, Inc.",
                                                     type: .misc,
                                                     bleConfig: nil,
                                                     mfiConfig: MFIConfig(data: "com.skullcandy.data", audio: "com.skullcandy.audio", race: "com.skullcandy.race"),
                                                     primaryConnectionServiceType: .mfi,
                                                     features: anySkullcandyFeatures)

    /// DeviceConfig for any Skullcandy device based on BLE manufacturer data
    static let anySkullcandyBLEDevice = DeviceConfig(manufacturerDescription: "Skullcandy, Inc.",
                                                     modelDescription: "Any Skullcandy, Inc.",
                                                     type: .misc,
                                                     bleConfig: BLEConfig(companyIdData: Data([0xC9, 0x07]), modelIdDataRange: nil, modelIdData: nil, localName: nil),
                                                     mfiConfig: nil,
                                                     primaryConnectionServiceType: .ble,
                                                     features: anySkullcandyFeatures)

    /// Features for any Skullcandy device
    static var anySkullcandyFeatures: [Feature] {
        return [Feature(type: .session, serviceType: .unknown)]
    }

}
