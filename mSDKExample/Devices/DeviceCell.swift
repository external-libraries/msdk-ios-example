//
// Created by Akos Polster on 23/04/2022.
// Copyright (C) 2022 Bragi. All rights reserved.
//

import UIKit
import mSDK

/// Cell representing a discovered device
class DeviceCell: UITableViewCell {
    var device: Device? {
        didSet {
            textLabel?.text = device?.longName
            detailTextLabel?.text = device?.identifier
            accessoryType = (device == nil) ? .none : .disclosureIndicator
        }
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
