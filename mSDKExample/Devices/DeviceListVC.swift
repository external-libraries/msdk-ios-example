//
// Created by Akos Polster on 11/04/2022.
// Copyright (C) 2022 Bragi. All rights reserved.
//

import UIKit
import RxSwift
import mSDK

/// Shows the list of discovered devices
class DeviceListVC: UIViewController, UITableViewDelegate {
    override func viewDidLoad() {
        super.viewDidLoad()
        let discoverButton = UIBarButtonItem(image: UIImage(systemName: "arrow.counterclockwise"), style: .plain, target: self, action: #selector(discover))
        navigationItem.rightBarButtonItem = discoverButton
        navigationItem.title = SystemInfo.shared.appName
        dataSource = DeviceListDataSource(tableView: tableView)
        tableView.dataSource = dataSource
        view.backgroundColor = .systemBackground
        view.addSubviewsForAutolayout(tableView)
        tableView.anchorEdges(to: view)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
        discover()
    }

    // MARK: - UITableViewDelegate

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let device = dataSource?.device(at: indexPath) else { return }
        if let cachedInteractor = deviceInteractors[device]?.value, cachedInteractor.isConnectedAndStarted {
            let deviceVC = DeviceVC(deviceInteractor: cachedInteractor)
            navigationController?.pushViewController(deviceVC, animated: true)
            return
        }
        let progressHUD = UIAlertController.makeProgressHUD(title: "Connecting…")
        present(progressHUD, animated: true)
        Bragi.shared
            .connectAndStartDevice(device)
            .do(onSuccess: { [weak self] deviceInteractor in
                progressHUD.dismiss(animated: true)
                self?.deviceInteractors[device] = WeakDeviceInteractor(value: deviceInteractor)
                let deviceVC = DeviceVC(deviceInteractor: deviceInteractor)
                self?.navigationController?.pushViewController(deviceVC, animated: true)
            }, onError: { [weak self] error in
                progressHUD.dismiss(animated: true)
                Logger.logError("\(error)")
                self?.present(UIAlertController.makeSimple(title: "Connection Failed", message: "\(error)"), animated: true)
            })
            .subscribe()
            .disposed(by: disposeBag)
    }

    // MARK: - Internal

    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero)
        tableView.delegate = self
        return tableView
    }()
    private var dataSource: DeviceListDataSource?
    private lazy var disposeBag = DisposeBag()
    private class WeakDeviceInteractor {
        weak var value: IDeviceInteractor?
        init(value: IDeviceInteractor) { self.value = value }
    }
    private var deviceInteractors = [Device: WeakDeviceInteractor]()

    @objc private func discover() {
        dataSource?.discover()
    }
}
