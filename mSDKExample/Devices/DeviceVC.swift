//
// Created by Akos Polster on 29/04/2022.
// Copyright (C) 2022 Bragi. All rights reserved.
//

import UIKit
import mSDK
import RxSwift

/// Presents a device
class DeviceVC: UIViewController {
    init(deviceInteractor: IDeviceInteractor) {
        self.deviceInteractor = deviceInteractor
        self.featureVCs = [
            InfoVC(deviceInteractor: deviceInteractor),
            TWSVC(deviceInteractor: deviceInteractor),
            SystemVC(deviceInteractor: deviceInteractor),
            AudioVC(deviceInteractor: deviceInteractor),
            TransparencyVC(deviceInteractor: deviceInteractor),
            EqualizerVC(deviceInteractor: deviceInteractor),
            UIVC(deviceInteractor: deviceInteractor),
            FOTAVC(deviceInteractor: deviceInteractor)
        ]
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        let scrollView = UIScrollView()
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.backgroundColor = .systemGroupedBackground
        scrollView.addSubviewsForAutolayout(stackView)
        view.addSubviewsForAutolayout(scrollView)
        scrollView.anchorEdges(to: view.safeAreaLayoutGuide)
        let cg = scrollView.contentLayoutGuide
        let fg = scrollView.frameLayoutGuide
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: cg.topAnchor),
            stackView.leadingAnchor.constraint(equalTo: cg.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: cg.trailingAnchor),
            stackView.bottomAnchor.constraint(equalTo: cg.bottomAnchor),
            stackView.widthAnchor.constraint(equalTo: fg.widthAnchor),
        ])
        stackView.addArrangedSubview(errorView)
        for featureVC in featureVCs {
            addChild(featureVC)
            stackView.addArrangedSubview(featureVC.view)
            featureVC.didMove(toParent: self)
        }
        let reloadButton = UIBarButtonItem(image: UIImage(systemName: "arrow.counterclockwise"), style: .plain, target: self, action: #selector(renderFeatures))
        navigationItem.rightBarButtonItem = reloadButton
        navigationItem.title = deviceInteractor?.device.shortName
    }

    /// Connect to the device, then display basic information about it
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        renderFeatures()
    }

    // MARK: - Internal

    private weak var deviceInteractor: IDeviceInteractor?
    private var startDeviceDisposable: Disposable?
    private lazy var disposeBag = DisposeBag()
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.isLayoutMarginsRelativeArrangement = true
        stackView.directionalLayoutMargins = NSDirectionalEdgeInsets(top: 0, leading: 0, bottom: 10, trailing: 0)
        return stackView
    }()
    private let featureVCs: [FeatureVC]
    private lazy var errorView = ErrorView()

    @objc private func renderFeatures() {
        guard deviceInteractor?.isConnectedAndStarted == true else {
            errorView.text = "\nDisconnected"
            return
        }
        errorView.text = nil
        for featureVC in featureVCs {
            featureVC.render()
        }
    }
}
