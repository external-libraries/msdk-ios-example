//
// Created by Tetiana Chunikhina on 31.05.2022
// Copyright (C) 2022 Bragi. All rights reserved.
//

import UIKit
import mSDK
import mSDKCore
import RxSwift
import RxBragi

class UIVC: FeatureVC {
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Action Buttons"
        contentView.addSubviewsForAutolayout(
            sideLabel,
            sideToggle,
            gestureLabel,
            gestureToggle,
            actionLabel,
            actionStackView
        )

        NSLayoutConstraint.activate([
            sideLabel.topAnchor.constraint(equalTo: contentView.topAnchor),
            sideLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            sideLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),

            sideToggle.topAnchor.constraint(equalTo: sideLabel.bottomAnchor, constant: 5),
            sideToggle.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            sideToggle.heightAnchor.constraint(equalToConstant: 40),
            sideToggle.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),

            gestureLabel.topAnchor.constraint(equalTo: sideToggle.bottomAnchor, constant: 10),
            gestureLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            gestureLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),

            gestureToggle.topAnchor.constraint(equalTo: gestureLabel.bottomAnchor, constant: 5),
            gestureToggle.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            gestureToggle.heightAnchor.constraint(equalToConstant: 40),
            gestureToggle.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),

            actionLabel.topAnchor.constraint(equalTo: gestureToggle.bottomAnchor, constant: 10),
            actionLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            actionLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),

            actionStackView.topAnchor.constraint(equalTo: actionLabel.bottomAnchor, constant: 5),
            actionStackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            actionStackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),

            contentView.bottomAnchor.constraint(equalTo: actionStackView.bottomAnchor),
        ])
        sideToggle.selectedSegmentIndex = 0
        gestureToggle.selectedSegmentIndex = 0
    }

    override func render() {
        super.render()
        guard
            let deviceInteractor = deviceInteractor,
            deviceInteractor.isConnectedAndStarted,
            deviceInteractor.isFeatureAvailable(.ui)
        else {
            sideLabel.text = "Feature is not available"
            return
        }
        self.renderActions()
    }

    // MARK: - Internal

    private lazy var sideLabel = MultiLineLabel(text: "Side")
    private lazy var gestureLabel = MultiLineLabel(text: "Gesture")
    private lazy var actionLabel = MultiLineLabel(text: "Action")

    private lazy var sideToggle: UISegmentedControl = {
        let control = UISegmentedControl(items: ["Left", "Right"])
        control.addTarget(self, action: #selector(setSide), for: .valueChanged)
        return control
    }()

    private lazy var gestureToggle: UISegmentedControl = {
        let control = UISegmentedControl(items: ["Click", "Double click", "Triple click", "Hold"])
        control.addTarget(self, action: #selector(setGesture), for: .valueChanged)
        return control
    }()

    var items = [
        ActionItem(option: .activeOff, title: "Active Off", isSelected: false),
        ActionItem(option: .telephonyOff, title: "Telephony Off", isSelected: false),
        ActionItem(option: .toggleAncTransparency, title: "toggle Anc Transparency", isSelected: false),
        ActionItem(option: .volumeUp, title: "Volume Up", isSelected: false),
        ActionItem(option: .volumeDown, title: "Volume Down", isSelected: false),
        ActionItem(option: .playPause, title: "Play Pause", isSelected: false),
        ActionItem(option: .nextTrack, title: "Next Track", isSelected: false),
        ActionItem(option: .previousTrack, title: "Previous Track", isSelected: false),
        ActionItem(option: .takeCall, title: "Take Call", isSelected: false),
        ActionItem(option: .rejectCall, title: "Reject Call", isSelected: false),
        ActionItem(option: .endCall, title: "End Call", isSelected: false),
        ActionItem(option: .takeEndCall, title: "Take End Call", isSelected: false),
        ActionItem(option: .rejectEndCall, title: "Reject End Call", isSelected: false),
        ActionItem(option: .error, title: "Error", isSelected: false),
        ActionItem(option: .fastMuteToggle, title: "Fast Mute Toggle", isSelected: false),
        ActionItem(option: .remoteUp, title: "Remote Up", isSelected: false),
        ActionItem(option: .voiceAssistant, title: "Voice Assistant", isSelected: false),
        ActionItem(option: .spotifyOneTouch, title: "Spotify One Touch", isSelected: false),
        ActionItem(option: .remoteSelfie, title: "Remote Selfie", isSelected: false)
    ]

    private lazy var actionStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        return stack
    }()

    private func selectView(_ selectedView: ActionItemView, selectedItem: ActionItem) {
        let selectedItemIndex = items.firstIndex { $0.title == selectedItem.title }
        for i in 0..<items.count {
            var item = items[i]
            item.isSelected = selectedItemIndex == i
            items[i] = item
        }

        actionStackView.arrangedSubviews.forEach { view in
            guard let actionView = view as? ActionItemView else {
                return
            }
            actionView.setSelected(actionView == selectedView)
        }
    }

    private var currentSide = UI.Side.left
    private var currentGesture = UI.Gesture.click

    private lazy var disposeBag = DisposeBag()

    @objc private func setSide() {
        switch sideToggle.selectedSegmentIndex {
        case 0:
            self.currentSide = .left
        case 1:
            self.currentSide = .right
        default:
            self.currentSide = .left
        }
        self.renderActions()
    }

    @objc private func setGesture() {
        switch gestureToggle.selectedSegmentIndex {
        case 0:
            self.currentGesture = .click
        case 1:
            self.currentGesture = .click2
        case 2:
            self.currentGesture = .click3
        case 3:
            self.currentGesture = .hold
        default:
            self.currentGesture = .click
        }
        self.renderActions()
    }

    private func renderActions() {
        deviceInteractor?.api.ui.getUsedCommands(config: self.currentSide, key: .power, gesture: self.currentGesture)
            .subscribe(onSuccess: { [weak self] command in
                self?.mapCommands(commands: command)
                self?.renderStackView()
                Logger.log("\(command)")
            }, onError: { error in
                Logger.logError("\(error)")
            })
            .disposed(by: disposeBag)
    }

    private func setAction(item: ActionItem) {
        let commands = mapItem(item: item)
        deviceInteractor?.api.ui.setCommands(config: self.currentSide, key: .power, gesture: self.currentGesture, commands: commands)
            .subscribe(onCompleted: {
                Logger.log("\(commands)")
            }, onError: { error in
                Logger.logError("\(error)")
            })
            .disposed(by: disposeBag)
    }

    private func mapItem(item: ActionItem) -> [UI.Command] {
        if item.option == .activeOff {
            return [.deviceNoAction]
        } else {
            return [item.option].compactMap { $0.command }
        }
    }

    private func mapCommands(commands: [UI.Command]) {
        let actionButtons = commands.compactMap { ActionButtonType.make($0) }
        for i in 0..<items.count {
            var item = items[i]
            for actionButton in actionButtons {
                if actionButton == item.option {
                    item.isSelected = true
                } else {
                    item.isSelected = false
                }
            }
            if actionButtons.isEmpty {
                item.isSelected = false
            }
            items[i] = item
        }
    }

    private func renderStackView() {
        actionStackView.removeAllArrangedSubviews()
        items.forEach { item in
            let itemView = ActionItemView(item: item)
            itemView.onTap = { [weak self] in
                self?.selectView(itemView, selectedItem: item)
                self?.setAction(item: item)
            }
            actionStackView.addArrangedSubview(itemView)
        }
    }
}
