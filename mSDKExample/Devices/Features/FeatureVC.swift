//
// Created by Akos Polster on 07/05/2022
// Copyright (C) 2022 Bragi. All rights reserved.
//

import UIKit
import mSDK

/// Renders a device feature
class FeatureVC: UIViewController {
    weak var deviceInteractor: IDeviceInteractor?
    let contentView = UIView()

    private let titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.preferredFont(forTextStyle: .headline)
        return label
    }()

    init(deviceInteractor: IDeviceInteractor) {
        self.deviceInteractor = deviceInteractor
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func render() {
    }

    override var title: String? {
        get { titleLabel.text }
        set { titleLabel.text = newValue }
    }

    override func loadView() {
        super.loadView()
        view = UIView()
        view.backgroundColor = .clear
        let cardView = UIView()
        cardView.backgroundColor = .secondarySystemGroupedBackground
        cardView.layer.cornerRadius = 10
        cardView.layer.masksToBounds = true
        let reloadButton = UIButton()
        reloadButton.setImage(UIImage(systemName: "arrow.counterclockwise"), for: .normal)
        reloadButton.addTarget(self, action: #selector(reload), for: .touchUpInside)
        view.addSubviewsForAutolayout(cardView, titleLabel, contentView, reloadButton)
        NSLayoutConstraint.activate([
            cardView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10),
            cardView.topAnchor.constraint(equalTo: view.topAnchor, constant: 10),
            cardView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10),

            reloadButton.topAnchor.constraint(equalTo: cardView.topAnchor, constant: 5),
            reloadButton.trailingAnchor.constraint(equalTo: cardView.trailingAnchor, constant: -5),
            reloadButton.widthAnchor.constraint(equalToConstant: 35),
            reloadButton.heightAnchor.constraint(equalToConstant: 35),

            titleLabel.leadingAnchor.constraint(equalTo: cardView.leadingAnchor, constant: 5),
            titleLabel.trailingAnchor.constraint(equalTo: reloadButton.leadingAnchor),
            titleLabel.centerYAnchor.constraint(equalTo: reloadButton.centerYAnchor),

            contentView.leadingAnchor.constraint(equalTo: cardView.leadingAnchor, constant: 5),
            contentView.trailingAnchor.constraint(equalTo: cardView.trailingAnchor, constant: -5),
            contentView.topAnchor.constraint(equalTo: reloadButton.bottomAnchor, constant: 5),

            cardView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 5),
            view.bottomAnchor.constraint(equalTo: cardView.bottomAnchor)
        ])
    }

    @objc private func reload() {
        render()
    }
}
