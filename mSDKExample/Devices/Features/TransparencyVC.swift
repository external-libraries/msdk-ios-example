//
// Created by Akos Polster on 07/05/2022
// Copyright (C) 2022 Bragi. All rights reserved.
//

import UIKit
import mSDK
import RxSwift
import RxBragi

/// Presents the audio transparency feature
class TransparencyVC: FeatureVC {
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Transparency"
        let transparencyTypeLabel = UILabel()
        transparencyTypeLabel.text = "Transparency type"
        contentView.addSubviewsForAutolayout(
            transparencyLabel,
            transparencyStateToggle,
            transparencyTypeLabel, transparencyTypeToggle,
            transparencySlider,
            atOnCallToggle,
            atOnMusicToggle
        )
        NSLayoutConstraint.activate([
            transparencyLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            transparencyLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            transparencyLabel.topAnchor.constraint(equalTo: contentView.topAnchor),

            transparencyStateToggle.topAnchor.constraint(equalTo: transparencyLabel.bottomAnchor, constant: 5),
            transparencyStateToggle.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            transparencyStateToggle.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),

            transparencyTypeToggle.topAnchor.constraint(equalTo: transparencyStateToggle.bottomAnchor, constant: 5),
            transparencyTypeToggle.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            transparencyTypeToggle.widthAnchor.constraint(equalToConstant: 180),
            transparencyTypeLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            transparencyTypeLabel.trailingAnchor.constraint(equalTo: transparencyTypeToggle.leadingAnchor),
            transparencyTypeLabel.centerYAnchor.constraint(equalTo: transparencyTypeToggle.centerYAnchor),

            transparencySlider.topAnchor.constraint(equalTo: transparencyTypeToggle.bottomAnchor, constant: 5),
            transparencySlider.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            transparencySlider.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),

            atOnCallToggle.topAnchor.constraint(equalTo: transparencySlider.bottomAnchor, constant: 5),
            atOnCallToggle.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            atOnCallToggle.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),

            atOnMusicToggle.topAnchor.constraint(equalTo: atOnCallToggle.bottomAnchor, constant: 5),
            atOnMusicToggle.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            atOnMusicToggle.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),

            contentView.bottomAnchor.constraint(equalTo: atOnMusicToggle.bottomAnchor)
        ])
    }

    override func render() {
        super.render()
        guard deviceInteractor?.isFeatureAvailable(.transparency) == true else {
            transparencyLabel.text = "Feature not available"
            return
        }
        renderTransparency()
    }

    // MARK: - Internal

    private lazy var transparencyLabel = MultiLineLabel()
    private lazy var transparencyStateToggle = Toggle(title: "Transparency state (old API)", valueChangeHandler: setTransparencyState)
    private lazy var transparencySlider = Slider(title: "Transparency gain") { _ in
        self.setTransparency()
    }
    private lazy var transparencyTypeToggle: UISegmentedControl = {
        let control = UISegmentedControl(items: ["Off", "1", "2", "3"])
        control.addTarget(self, action: #selector(setTransparency), for: .valueChanged)
        return control
    }()
    private lazy var atOnCallToggle = Toggle(title: "Adaptive Transparency on call", valueChangeHandler: setATOnCall)
    private lazy var atOnMusicToggle = Toggle(title: "Adaptive Transparency on music", valueChangeHandler: setATOnMusic)
    private lazy var disposeBag = DisposeBag()
    private var transparencyDisposable: Disposable?
    private var adaptiveTransparencyDisposable: Disposable?
    private static let gainExtremum = 20.0

    private func renderTransparency() {
        guard let deviceInteractor = deviceInteractor else { return }

        transparencyDisposable?.dispose()
        transparencyDisposable = deviceInteractor.api.audio.getTransparency()
            .subscribe(onSuccess: { [weak self] transparency in
                guard let self = self else { return }
                let value = self.transparencyGainToSliderValue(gain: transparency.value)
                let text = "\(transparency.state), \(transparency.value) (\(value)%)"
                Logger.log(text)
                self.transparencyLabel.text = text
                self.transparencySlider.setValue(Float(value), animated: true)
                if transparency.state.isOff {
                    self.transparencyTypeToggle.selectedSegmentIndex = 0
                }
                self.transparencyStateToggle.setOn(transparency.state.isOn, animated: true)
            }, onError: { [weak self] error in
                Logger.logError("\(error)")
                self?.transparencyLabel.text = "Error: \(error)"
            })
        transparencyDisposable?.disposed(by: disposeBag)

        let getATOnCall = deviceInteractor.api.audio.getATOnCall()
            .map { $0.isOn }
            .catchError {
                Logger.logError("\($0)")
                return .just(false)
            }
            .asObservable()
        let getATOnMusic = deviceInteractor.api.audio.getATOnMusic()
            .map { $0.isOn }
            .catchError {
                Logger.logError("\($0)")
                return .just(false)
            }
            .asObservable()

        adaptiveTransparencyDisposable?.dispose()
        adaptiveTransparencyDisposable = Observable
            .zip(getATOnCall, getATOnMusic)
            .subscribe { [weak self] atOnCall, atOnMusic in
                self?.atOnCallToggle.setOn(atOnCall, animated: true)
                self?.atOnMusicToggle.setOn(atOnMusic, animated: true)
            }
        adaptiveTransparencyDisposable?.disposed(by: disposeBag)
    }

    @objc private func setTransparency() {
        Logger.log("Setting to type \(transparencyTypeToggle.selectedSegmentIndex), gain \(Int(transparencySlider.value))%")
        let gain = sliderValueToTransparencyGain(value: transparencySlider.value)
        let filter: Filter
        switch transparencyTypeToggle.selectedSegmentIndex {
        case 1: filter = .passthrough1(gain: gain)
        case 2: filter = .passthrough2(gain: gain)
        case 3: filter = .passthrough3(gain: gain)
        default: filter = .off
        }
        deviceInteractor?.api.audio
            .setTransparency(filter)
            .subscribe(onCompleted: { [weak self] in
                self?.renderTransparency()
            }, onError: { [weak self] error in
                Logger.logError("\(error)")
                self?.transparencyLabel.text = "Error: \(error)"
            })
            .disposed(by: disposeBag)
    }

    private func setTransparencyState(_ isOn: Bool) {
        Logger.log("\(isOn)")
        let state = Utils.State.fromBool(isOn)
        deviceInteractor?.api.audio
            .setTransparencyState(state)
            .subscribe(onCompleted: { [weak self] in
                self?.renderTransparency()
            }, onError: { [weak self] error in
                Logger.logError("\(error)")
                self?.transparencyLabel.text = "Error: \(error)"
            })
            .disposed(by: disposeBag)
    }

    private func setATOnCall(_ isOn: Bool) {
        Logger.log("\(isOn)")
        deviceInteractor?.api.audio
            .setATOnCall(.fromBool(isOn))
            .subscribe(onCompleted: { [weak self] in
                self?.renderTransparency()
            }, onError: { [weak self] error in
                Logger.logError("\(error)")
                self?.transparencyLabel.text = "Error: \(error)"
                self?.atOnCallToggle.setOn(false, animated: true)
            })
            .disposed(by: disposeBag)
    }

    @objc private func setATOnMusic(_ isOn: Bool) {
        Logger.log("\(isOn)")
        deviceInteractor?.api.audio
            .setATOnMusic(.fromBool(isOn))
            .subscribe(onCompleted: { [weak self] in
                self?.renderTransparency()
            }, onError: { [weak self] error in
                Logger.logError("\(error)")
                self?.transparencyLabel.text = "Error: \(error)"
                self?.atOnMusicToggle.setOn(false, animated: true)
            })
            .disposed(by: disposeBag)
    }

    private func transparencyGainToSliderValue(gain: Int) -> Float {
        Float((Double(gain) + Self.gainExtremum) * (100.0 / Self.gainExtremum))
    }

    private func sliderValueToTransparencyGain(value: Float) -> Double {
        (Double(value) - 100) / (100 / Self.gainExtremum)
    }
}
