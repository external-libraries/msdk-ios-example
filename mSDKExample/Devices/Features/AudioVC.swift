//
// Created by Akos Polster on 07/05/2022
// Copyright (C) 2022 Bragi. All rights reserved.
//

import UIKit
import mSDK
import RxSwift
import RxBragi

/// Presents the audio feature
class AudioVC: FeatureVC {
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Audio"
        let ancGainTypeLabel = UILabel()
        ancGainTypeLabel.text = "ANC type"
        contentView.addSubviewsForAutolayout(
            audioInfoLabel,
            trackLabel, hearingChangeLabel,
            volumeSlider,
            volumeUpButton, volumeDownButton,
            ancGainTypeLabel, ancGainTypeToggle,
            ancGainSlider,
            ancStateSwitch,
            autoEQSwitch
        )
        NSLayoutConstraint.activate([
            audioInfoLabel.topAnchor.constraint(equalTo: contentView.topAnchor),
            audioInfoLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            audioInfoLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),

            trackLabel.topAnchor.constraint(equalTo: audioInfoLabel.bottomAnchor, constant: 5),
            trackLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            trackLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),

            hearingChangeLabel.topAnchor.constraint(equalTo: trackLabel.bottomAnchor),
            hearingChangeLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            hearingChangeLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),

            volumeSlider.topAnchor.constraint(equalTo: hearingChangeLabel.bottomAnchor, constant: 10),
            volumeSlider.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            volumeSlider.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),

            volumeUpButton.topAnchor.constraint(equalTo: volumeSlider.bottomAnchor, constant: 5),
            volumeUpButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            volumeUpButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),

            volumeDownButton.topAnchor.constraint(equalTo: volumeUpButton.bottomAnchor, constant: 5),
            volumeDownButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            volumeDownButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),

            ancStateSwitch.topAnchor.constraint(equalTo: volumeDownButton.bottomAnchor, constant: 5),
            ancStateSwitch.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            ancStateSwitch.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),

            ancGainTypeToggle.topAnchor.constraint(equalTo: ancStateSwitch.bottomAnchor, constant: 10),
            ancGainTypeToggle.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            ancGainTypeToggle.widthAnchor.constraint(equalToConstant: 180),
            ancGainTypeLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            ancGainTypeLabel.trailingAnchor.constraint(equalTo: ancGainTypeToggle.leadingAnchor),
            ancGainTypeLabel.centerYAnchor.constraint(equalTo: ancGainTypeToggle.centerYAnchor),

            ancGainSlider.topAnchor.constraint(equalTo: ancGainTypeToggle.bottomAnchor, constant: 5),
            ancGainSlider.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            ancGainSlider.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),

            autoEQSwitch.topAnchor.constraint(equalTo: ancGainSlider.bottomAnchor, constant: 5),
            autoEQSwitch.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            autoEQSwitch.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),

            contentView.bottomAnchor.constraint(equalTo: autoEQSwitch.bottomAnchor),
        ])
    }

    override func render() {
        super.render()
        guard
            let deviceInteractor = deviceInteractor,
            deviceInteractor.isConnectedAndStarted,
            deviceInteractor.isFeatureAvailable(.audio)
        else {
            audioInfoLabel.text = "Feature not available"
            return
        }
        renderAudioInfo()
        renderVolume()
        renderANC()
        renderAutoEQ()
    }

    // MARK: - Internal

    private lazy var audioInfoLabel = MultiLineLabel()
    private lazy var trackLabel = MultiLineLabel(text: "Track change: n/a")
    private lazy var hearingChangeLabel = MultiLineLabel(text: "Hearing change: n/a")
    private lazy var volumeSlider = Slider(title: "Volume", maximumValue: 10, valueChangeHandler: setVolume)
    private lazy var ancGainTypeToggle: UISegmentedControl = {
        let control = UISegmentedControl(items: ["Off", "1", "2", "3"])
        control.addTarget(self, action: #selector(setANCGain), for: .valueChanged)
        return control
    }()
    private lazy var ancGainSlider = Slider(title: "ANC gain") { [weak self] _ in
        self?.setANCGain()
    }
    private lazy var ancStateSwitch = Toggle(title: "ANC state (old API)", valueChangeHandler: setANCState)
    private lazy var volumeUpButton = BigButton(title: "Volume Up", action: increaseVolume)
    private lazy var volumeDownButton = BigButton(title: "Volume Down", action: decreaseVolume)
    private lazy var autoEQSwitch = Toggle(title: "Auto EQ", valueChangeHandler: setAutoEQ)
    private lazy var disposeBag = DisposeBag()
    private var audioInfoDisposable: Disposable?
    private var trackInfoDisposable: Disposable?
    private var hearingDisposable: Disposable?
    private var volumeDisposable: Disposable?
    private var ancDisposable: Disposable?
    private var ancStateDisposable: Disposable?
    private var autoEQDisposable: Disposable?
    private static let gainExtremum = 10.0

    private func renderAudioInfo() {
        guard let deviceInteractor = deviceInteractor else { return }
        audioInfoDisposable?.dispose()
        audioInfoDisposable = deviceInteractor.api.audio.getAudioSettings()
            .subscribe(onSuccess: { [weak self] audioSettings in
                self?.audioInfoLabel.text =
                    "Settings: " +
                    "\n  Volume: \(audioSettings.volume)" +
                    "\n  Sampling rate: \(audioSettings.samplingRate)" +
                    "\n  ANC state: \(audioSettings.ancState)" +
                    "\n  Recording state: \(audioSettings.recordingState)" +
                    "\n  Transparency: \(audioSettings.transparency.state) \(audioSettings.transparency.value)"
            }, onError: { [weak self] error in
                Logger.logError("\(error)")
                self?.audioInfoLabel.text =
                    "Settings: " +
                    "\n  Volume: n/a" +
                    "\n  Sampling rate: n/a" +
                    "\n  ANC state: n/a" +
                    "\n  Recording state: n/a" +
                    "\n  Transparency: n/a"
            })
        audioInfoDisposable?.disposed(by: disposeBag)

        trackInfoDisposable?.dispose()
        trackInfoDisposable = deviceInteractor.api.audio
            .observeTrackInfo()
            .subscribe(onNext: { [weak self] track in
                self?.trackLabel.text = "Track change: \(track.title)/\(track.artist)/\(track.genre)"
            }, onError: { [weak self] error in
                Logger.logError("\(error)")
                self?.trackLabel.text = "Track change: n/a"
            })
        trackInfoDisposable?.disposed(by: disposeBag)

        hearingDisposable?.dispose()
        hearingDisposable = deviceInteractor.api.audio
            .observeHearingChange()
            .subscribe(onNext: { [weak self] hearingChange in
                switch hearingChange {
                case .transparency(let state):
                    self?.hearingChangeLabel.text = "Hearing change: Transparency \(state)"
                case .anc(let state):
                    self?.hearingChangeLabel.text = "Hearing change: ANC \(state)"
                @unknown default:
                    self?.hearingChangeLabel.text = "Hearing change: \(hearingChange)"
                }
                self?.renderANC()
            }, onError: { [weak self] error in
                Logger.logError("\(error)")
                self?.hearingChangeLabel.text = "Hearing change: n/a"
                self?.renderANC()
            })
        hearingDisposable?.disposed(by: disposeBag)
    }

    private func renderVolume() {
        volumeDisposable?.dispose()
        volumeDisposable = deviceInteractor?.api.audio.getVolume()
            .subscribe(onSuccess: { [weak self] volume in
                Logger.log("\(volume)")
                self?.volumeSlider.value = Float(volume)
            }, onError: { [weak self] error in
                Logger.logError("\(error)")
                self?.volumeSlider.setValue(0, animated: true)
            })
        volumeDisposable?.disposed(by: disposeBag)
    }

    private func renderANC() {
        ancDisposable?.dispose()
        ancDisposable = deviceInteractor?.api.audio.getANCGain()
            .subscribe(onSuccess: { [weak self] filter in
                guard let self = self else { return }
                let gainValue: Double
                let toggleIndex: Int
                switch filter {
                case .anc1(let gain):
                    gainValue = gain
                    toggleIndex = 1
                case .anc2(let gain):
                    gainValue = gain
                    toggleIndex = 2
                case .anc3(let gain):
                    gainValue = gain
                    toggleIndex = 3
                default:
                    gainValue = 0
                    toggleIndex = 0
                }
                Logger.log("ANC type \(toggleIndex), gain \(gainValue)")
                self.ancGainSlider.value = self.ancGainToSliderValue(gain: gainValue)
                self.ancGainTypeToggle.selectedSegmentIndex = toggleIndex
            }, onError: { error in
                Logger.logError("\(error)")
            })

        ancStateDisposable?.dispose()
        ancStateDisposable = deviceInteractor?.api.audio
            .getANCState()
            .subscribe(onSuccess: { [weak self] state in
                self?.ancStateSwitch.setOn(state.isOn, animated: true)
            }, onError: { [weak self] error in
                self?.ancStateSwitch.setOn(false, animated: true)
                Logger.logError("\(error)")
            })
    }

    private func renderAutoEQ() {
        autoEQDisposable?.dispose()
        autoEQDisposable = deviceInteractor?.api.audio
            .getAutomaticEQState()
            .subscribe(onSuccess: { [weak self] state in
                self?.autoEQSwitch.setOn(state.isOn, animated: true)
            }, onError: { [weak self] error in
                self?.autoEQSwitch.setOn(false, animated: true)
                Logger.logError("\(error)")
            })
    }

    private func setAutoEQ(_ isOn: Bool) {
        deviceInteractor?.api.audio
            .setAutomaticEQState(.fromBool(isOn))
            .subscribe(onCompleted: { [weak self] in
                self?.renderAutoEQ()
            }, onError: { [weak self] error in
                Logger.logError("\(error)")
                self?.autoEQSwitch.setOn(false, animated: true)
            })
            .disposed(by: disposeBag)
    }

    private func increaseVolume() {
        Logger.log()
        deviceInteractor?.api.audio
            .volumeUp(by: 1)
            .subscribe(onCompleted: { [weak self] in
                self?.renderVolume()
            }, onError: { error in
                Logger.logError("\(error)")
            })
            .disposed(by: disposeBag)
    }

    private func decreaseVolume() {
        Logger.log()
        deviceInteractor?.api.audio
            .volumeDown(by: 1)
            .subscribe(onCompleted: { [weak self] in
                self?.renderVolume()
            }, onError: { error in
                Logger.logError("\(error)")
            })
            .disposed(by: disposeBag)
    }

    @objc private func setANCGain() {
        let gainValue = ancGainSlider.value
        let gain = sliderValueToANCGain(value: gainValue)
        let filter: Filter
        switch ancGainTypeToggle.selectedSegmentIndex {
        case 1: filter = .anc1(gain: gain)
        case 2: filter = .anc2(gain: gain)
        case 3: filter = .anc3(gain: gain)
        default: filter = .off
        }
        deviceInteractor?.api.audio
            .setANCGain(filter)
            .subscribe(onCompleted: { [weak self] in
                self?.renderANC()
            }, onError: { error in
                Logger.logError("\(error)")
            })
            .disposed(by: disposeBag)
    }

    private func setANCState(_ isOn: Bool) {
        deviceInteractor?.api.audio
            .setANCState(Utils.State.fromBool(isOn))
            .subscribe(onCompleted: { [weak self] in
                self?.renderANC()
            }, onError: { error in
                Logger.logError("\(error)")
            })
            .disposed(by: disposeBag)
    }

    @objc private func setVolume(_ value: Float) {
        Logger.log("\(value)")
        deviceInteractor?.api.audio
            .setVolume(Int(value))
            .subscribe(onCompleted: { [weak self] in
                self?.renderVolume()
            }, onError: { [weak self] error in
                Logger.logError("\(error)")
                self?.renderVolume()
            })
            .disposed(by: disposeBag)
    }

    private func ancGainToSliderValue(gain: Double) -> Float {
        Float((gain + Self.gainExtremum) * (100.0 / Self.gainExtremum))
    }

    private func sliderValueToANCGain(value: Float) -> Double {
        (Double(value) - 100) / (100 / Self.gainExtremum)
    }
}
