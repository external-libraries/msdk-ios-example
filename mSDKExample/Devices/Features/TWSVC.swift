//
// Created by Akos Polster on 07/05/2022
// Copyright (C) 2022 Bragi. All rights reserved.
//

import UIKit
import mSDK
import RxSwift
import RxBragi

/// Presents the TWS device feature
class TWSVC: FeatureVC {
    override func viewDidLoad() {
        super.viewDidLoad()
        contentView.addSubviewsForAutolayout(label)
        label.anchorEdges(to: contentView)
        title = "TWS Information"
    }

    override func render() {
        super.render()
        guard
            let deviceInteractor = deviceInteractor,
            deviceInteractor.isConnectedAndStarted,
            deviceInteractor.isFeatureAvailable(.tws)
        else {
            label.text = "Feature not available"
            return
        }
        twsDisposable?.dispose()
        twsDisposable = deviceInteractor.api.device
            .getTWSState()
            .do(onSuccess: { [weak self] state in
                self?.label.text = "State: \(state)"
                self?.label.sizeToFit()
            }, onError: { [weak self] error in
                Logger.logError("\(error)")
                self?.label.text = "\(error)"
                self?.label.sizeToFit()
            })
            .subscribe()
        twsDisposable?.disposed(by: disposeBag)
    }

    // MARK: - Internal

    private lazy var label = MultiLineLabel()
    private lazy var disposeBag = DisposeBag()
    private var twsDisposable: Disposable?
}
