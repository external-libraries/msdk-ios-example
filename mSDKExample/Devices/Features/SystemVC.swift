//
// Created by Akos Polster on 07/05/2022
// Copyright (C) 2022 Bragi. All rights reserved.
//

import UIKit
import mSDK
import RxSwift
import RxBragi

/// Presents the System API
class SystemVC: FeatureVC {
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "System"
        contentView.addSubviewsForAutolayout(
            infoLabel,
            bondableStateLabel,
            timeLabel,
            setTimeToCurrentButton,
            setTimeTo2025Button,
            resetSPPPDLButton,
            resetBLEPDLButton,
            factoryResetButton
        )
        NSLayoutConstraint.activate([
            infoLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            infoLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            infoLabel.topAnchor.constraint(equalTo: contentView.topAnchor),

            bondableStateLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            bondableStateLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            bondableStateLabel.topAnchor.constraint(equalTo: infoLabel.bottomAnchor),

            timeLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            timeLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            timeLabel.topAnchor.constraint(equalTo: bondableStateLabel.bottomAnchor),

            setTimeToCurrentButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            setTimeToCurrentButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            setTimeToCurrentButton.topAnchor.constraint(equalTo: timeLabel.bottomAnchor, constant: 5),

            setTimeTo2025Button.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            setTimeTo2025Button.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            setTimeTo2025Button.topAnchor.constraint(equalTo: setTimeToCurrentButton.bottomAnchor, constant: 5),

            resetSPPPDLButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            resetSPPPDLButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            resetSPPPDLButton.topAnchor.constraint(equalTo: setTimeTo2025Button.bottomAnchor, constant: 5),

            resetBLEPDLButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            resetBLEPDLButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            resetBLEPDLButton.topAnchor.constraint(equalTo: resetSPPPDLButton.bottomAnchor, constant: 5),

            factoryResetButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            factoryResetButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            factoryResetButton.topAnchor.constraint(equalTo: resetBLEPDLButton.bottomAnchor, constant: 5),

            contentView.bottomAnchor.constraint(equalTo: factoryResetButton.bottomAnchor)
        ])
    }

    override func render() {
        super.render()
        guard deviceInteractor?.isFeatureAvailable(.system) == true else {
            infoLabel.text = "Feature not available"
            setTimeTo2025Button.isEnabled = false
            setTimeToCurrentButton.isEnabled = false
            resetBLEPDLButton.isEnabled = false
            resetSPPPDLButton.isEnabled = false
            factoryResetButton.isEnabled = false
            return
        }
        infoLabel.text = nil
        setTimeTo2025Button.isEnabled = true
        setTimeToCurrentButton.isEnabled = true
        resetBLEPDLButton.isEnabled = true
        resetSPPPDLButton.isEnabled = true
        factoryResetButton.isEnabled = true
        getTimeDisposable?.dispose()
        getTimeDisposable = deviceInteractor?.api.system
            .getTime()
            .subscribe(onSuccess: { [weak self] time in
                self?.timeLabel.text = "Time: \(time)"
            }, onError: { [weak self] error in
                self?.timeLabel.text = "Time: \(error)"
            })
        getTimeDisposable?.disposed(by: disposeBag)

        bondableStateLabel.text = "Bondable: Observing"
        bondableStateDisposable?.dispose()
        bondableStateDisposable = deviceInteractor?.api.system
            .observeBondableState()
            .subscribe(onNext: { [weak self] milliseconds in
                self?.bondableStateLabel.text = "Bondable: In \(milliseconds) ms"
            }, onError: { [weak self] error in
                Logger.logError("\(error)")
                self?.bondableStateLabel.text = "Bondable: \(error)"
            })
        bondableStateDisposable?.disposed(by: disposeBag)
    }

    // MARK: - Internal

    private lazy var infoLabel = MultiLineLabel()
    private lazy var setTimeToCurrentButton = BigButton(title: "Set Time to Current") { [weak self] in self?.setTime() }
    private lazy var setTimeTo2025Button = BigButton(title: "Set Time to 2025") { [weak self] in self?.setTime("2025-05-05T05:05:05.005,5") }
    private lazy var factoryResetButton = BigButton(title: "Reset to Factory Settings", action: resetToFactorySettings)
    private lazy var resetSPPPDLButton = BigButton(title: "Reset Paired SPP Device List", action: resetSPPPDL)
    private lazy var resetBLEPDLButton = BigButton(title: "Reset Paired BLE Device List", action: resetBLEPDL)
    private lazy var timeLabel = MultiLineLabel()
    private lazy var bondableStateLabel = MultiLineLabel()
    private lazy var disposeBag = DisposeBag()
    private var getTimeDisposable: Disposable?
    private var bondableStateDisposable: Disposable?

    private func setTime(_ time: String? = nil) {
        let dateFormatter: DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.sss,e"
            return formatter
        }()
        let timeStr: String = time ?? dateFormatter.string(from: Date())
        Logger.log(timeStr)
        deviceInteractor?.api.system
            .setTime(timeStr)
            .subscribe(onCompleted: { [weak self] in
                self?.infoLabel.text = nil
                self?.render()
            }, onError: { [weak self] error in
                Logger.logError("\(error)")
                self?.infoLabel.text = "Set time: \(error)"
            })
            .disposed(by: disposeBag)
    }

    private func resetToFactorySettings() {
        let alert = UIAlertController(title: "Reset to Factory Settings?", message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Reset", style: .destructive, handler: doResetToFactorySettings))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        present(alert, animated: true)
    }

    private func resetSPPPDL() {
        let alert = UIAlertController(title: "Reset Paired SPP Device List?", message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Reset", style: .destructive) { [weak self] _ in self?.doResetPDL(connectionType: .spp) })
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        present(alert, animated: true)
    }

    private func resetBLEPDL() {
        let alert = UIAlertController(title: "Reset Paired BLE Device List?", message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Reset", style: .destructive) { [weak self] _ in self?.doResetPDL(connectionType: .ble) })
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        present(alert, animated: true)
    }

    private func doResetToFactorySettings(_ action: UIAlertAction) {
        Logger.log()
        deviceInteractor?.api.system
            .factoryReset()
            .subscribe(onCompleted: { [weak self] in
                self?.infoLabel.text = nil
                self?.adviseToReconnect()
            }, onError: { [weak self] error in
                Logger.logError("\(error)")
                // Factory reset may never complete -- treat error as success
                self?.infoLabel.text = nil
                self?.adviseToReconnect()
            })
            .disposed(by: disposeBag)
    }

    private func doResetPDL(connectionType: Utils.ConnectionType) {
        Logger.log("\(connectionType)")
        deviceInteractor?.api.system
            .resetPdl(connectionType)
            .subscribe(onCompleted: { [weak self] in
                self?.infoLabel.text = nil
                self?.render()
            }, onError: { [weak self] error in
                Logger.logError("\(error)")
                self?.infoLabel.text = "Reset PDL: \(error)"
            })
            .disposed(by: disposeBag)
    }

    private func adviseToReconnect() {
        let message: String
        if deviceInteractor?.device.isBLE == true {
            message = "Device disconnected. Go back to the previous screen and select the device again to reconnect"
        } else {
            message = "Device disconnected. Reconnect the device using the Settings app, then go back to the previous screen and select the device again to reconnect"
        }
        let alert = UIAlertController(title: "Reset Complete", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel))
        present(alert, animated: true)
    }
}
