//
// Created by Akos Polster on 07/05/2022
// Copyright (C) 2022 Bragi. All rights reserved.
//

import UIKit
import mSDK
import RxSwift
import RxBragi

/// Presents the equalizer feature
class EqualizerVC: FeatureVC {
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Equalizer"
        let eqLabel = UILabel()
        eqLabel.text = "Presets"
        contentView.addSubviewsForAutolayout(
            infoLabel,
            enableSwitch,
            eqLabel, eqToggle,
            detailsLabel
        )
        NSLayoutConstraint.activate([
            infoLabel.topAnchor.constraint(equalTo: contentView.topAnchor),
            infoLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            infoLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),

            enableSwitch.topAnchor.constraint(equalTo: infoLabel.bottomAnchor, constant: 5),
            enableSwitch.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            enableSwitch.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),

            eqToggle.topAnchor.constraint(equalTo: enableSwitch.bottomAnchor, constant: 5),
            eqToggle.widthAnchor.constraint(equalToConstant: 180),
            eqToggle.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            eqLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            eqLabel.trailingAnchor.constraint(equalTo: eqToggle.leadingAnchor),
            eqLabel.centerYAnchor.constraint(equalTo: eqToggle.centerYAnchor),

            detailsLabel.topAnchor.constraint(equalTo: eqToggle.bottomAnchor, constant: 5),
            detailsLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            detailsLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),

            contentView.bottomAnchor.constraint(equalTo: detailsLabel.bottomAnchor),
        ])
    }

    override func render() {
        super.render()
        guard deviceInteractor?.isFeatureAvailable(.equalizer) == true else {
            infoLabel.text = "Feature not available"
            return
        }
        renderEQ()
    }

    // MARK: - Internal

    private lazy var infoLabel = MultiLineLabel()
    private lazy var eqToggle: UISegmentedControl = {
        let control = UISegmentedControl()
        control.addTarget(self, action: #selector(setEQ), for: .valueChanged)
        return control
    }()
    private lazy var detailsLabel = MultiLineLabel()
    private lazy var enableSwitch = Toggle(title: "Enabled", valueChangeHandler: enableEQ)
    private lazy var disposeBag = DisposeBag()
    private var eqDisposable: Disposable?
    private var currentPresets = [Preset]()

    private func renderEQ() {
        guard let deviceInteractor = deviceInteractor else { return }
        eqDisposable?.dispose()
        eqDisposable = Observable
            .zip(
                deviceInteractor.api.equalizer.getPresets().asObservable(),
                deviceInteractor.api.equalizer.getActivePreset().asObservable()
            )
            .subscribe { [weak self] presets, activePreset in
                Logger.log("Presets: \(presets), active: \(String(describing: activePreset))")
                guard let self = self else { return }
                self.infoLabel.text = nil
                self.eqToggle.removeAllSegments()
                var details = ""
                self.currentPresets = presets
                    .sorted { left, right -> Bool in
                        switch (left.index, right.index) {
                        case (.custom(let numberLeft), .custom(let numberRight)): return numberLeft < numberRight
                        case (.default(let numberLeft), .default(let numberRight)): return numberLeft < numberRight
                        case (.default, .custom): return true
                        default: return false
                        }
                    }
                self.currentPresets
                    .reversed()
                    .forEach { preset in
                        let title: String
                        switch preset.index {
                        case .custom(let number): title = "C\(number)"
                        case .default(let number): title = "\(number)"
                        }
                        self.eqToggle.insertSegment(withTitle: title, at: 0, animated: false)
                        if activePreset?.index == preset.index {
                            self.eqToggle.selectedSegmentIndex = 0
                        }
                        let bandDetails = preset.body.bands
                            .map { "  \($0.frequency) Hz: gain \($0.gain), q \($0.qualityFactor)" }
                            .joined(separator: "\n")
                        if !bandDetails.isEmpty {
                            details = "Preset \(title):\n" + bandDetails + "\n" + details
                        }
                    }
                self.detailsLabel.text = details
                self.enableSwitch.setOn(activePreset != nil, animated: true)
            }
        eqDisposable?.disposed(by: disposeBag)
    }

    @objc private func setEQ() {
        let presetIndex = (eqToggle.selectedSegmentIndex == -1) ? 0 : eqToggle.selectedSegmentIndex
        guard let preset = currentPresets[safe: presetIndex] else { return }
        deviceInteractor?.api.equalizer
            // TODO: setPreset doesn't work with custom EQs -- Investigate why
            // .setPreset(preset)
            .selectPreset(at: preset.index)
            .subscribe(onCompleted: { [weak self] in
                self?.renderEQ()
            }, onError: { [weak self] error in
                Logger.logError("\(error)")
                self?.infoLabel.text = "Set EQ: \(error)"
            })
            .disposed(by: disposeBag)
    }

    private func enableEQ(_ isOn: Bool) {
        if isOn {
            setEQ()
        } else {
            deviceInteractor?.api.equalizer
                .disableEQ()
                .subscribe(onCompleted: { [weak self] in
                    self?.renderEQ()
                }, onError: { [weak self] error in
                    Logger.logError("\(error)")
                    self?.infoLabel.text = "Enable EQ: \(error)"
                })
                .disposed(by: disposeBag)
        }
    }
}
