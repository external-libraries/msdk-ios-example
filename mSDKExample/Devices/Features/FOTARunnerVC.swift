//
// Created by Akos Polster on 04/05/2022
// Copyright (C) 2022 Bragi. All rights reserved.
//

import UIKit
import MobileCoreServices
import mSDK
import RxSwift
import RxBragi

/// Updates firmware over the air
class FOTARunnerVC: UIViewController, UIDocumentPickerDelegate {
    init(deviceInteractor: IDeviceInteractor?) {
        self.deviceInteractor = deviceInteractor
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Update Firmware"
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Close", style: .plain, target: self, action: #selector(close))
        view.backgroundColor = .systemBackground
        fotaTypeToggle.selectedSegmentIndex = 0

        view.addSubviewsForAutolayout([scrollView, stackView])
        scrollView.addSubviewsForAutolayout(stackView)

        let heightConstraint = stackView.heightAnchor.constraint(equalTo: view.heightAnchor)
        heightConstraint.priority = .defaultLow
        NSLayoutConstraint.activate([
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            scrollView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),

            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 5),

            stackView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 10),
            stackView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 10),
            stackView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: -10),
            stackView.widthAnchor.constraint(equalToConstant: view.frame.width - 20),
            stackView.bottomAnchor.constraint(lessThanOrEqualTo: scrollView.bottomAnchor, constant: 0),

            view.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            heightConstraint
        ])
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        refreshUI()
    }

    // MARK: - UIDocumentPickerDelegate

    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let url = urls.first else { return }
        Logger.log("\(url)")
        firmwareURL = url
        switch self.buttonType {
        case .left:
            localLeftFirmwareURL = makeLocalCopy(firmwareURL: url)
        case .right:
            localRightFirmwareURL = makeLocalCopy(firmwareURL: url)
        case .dual:
            localFirmwareURL = makeLocalCopy(firmwareURL: url)
        }
        refreshUI()
    }

    // MARK: - Internal

    private weak var deviceInteractor: IDeviceInteractor?
    private var firmwareURL: URL?
    private var localFirmwareURL: URL?
    private var buttonType = ButtonType.dual
    private var localLeftFirmwareURL: URL?
    private var localRightFirmwareURL: URL?

    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.showsVerticalScrollIndicator = false
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()

    private lazy var stackView: UIStackView = {
        let stack = UIStackView(arrangedSubviews: [fotaTypeToggle, pickFirmwareButton, firmwareLabel, startFOTAButton, progressLabel, errorLabel])
        stack.axis = .vertical
        stack.spacing = 10
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()

    private lazy var fotaTypeToggle: UISegmentedControl = {
        let control = UISegmentedControl(items: ["Use one file", "Use two files"])
        control.addTarget(self, action: #selector(setFOTAType), for: .valueChanged)
        return control
    }()

    private lazy var pickFirmwareButton = BigButton(title: "Select Firmware File") { [weak self] in self?.pickFirmware(.dual) }

    private lazy var pickLeftFOTAFirmwareButton = BigButton(title: "Select Left Firmware File") { [weak self] in self?.pickFirmware(.left) }

    private lazy var pickRightFOTAFirmwareButton = BigButton(title: "Select Right Firmware File") { [weak self] in self?.pickFirmware(.right) }

    private lazy var startFOTAButton = BigButton(title: "Update Firmware") { [weak self] in self?.startFOTA() }

    private let firmwareLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()

    private let progressLabel = UILabel()
    private let errorLabel: MultiLineLabel = {
        let label = MultiLineLabel()
        label.textColor = .systemRed
        return label
    }()

    private lazy var disposeBag = DisposeBag()
    private var fotaDisposable: Disposable?
    private let activityIndicator = UIActivityIndicatorView(style: .large)

    private enum ButtonType {
        case left
        case right
        case dual
    }

    private func pickFirmware(_ buttonType: ButtonType) {
        self.buttonType = buttonType
        let documentPicker = UIDocumentPickerViewController(documentTypes: [kUTTypeItem as String], in: .import)
        documentPicker.delegate = self
        self.present(documentPicker, animated: true, completion: nil)
    }

    private func makeLocalCopy(firmwareURL: URL) -> URL? {
        _ = firmwareURL.startAccessingSecurityScopedResource()
        defer { firmwareURL.stopAccessingSecurityScopedResource() }

        let cacheURL = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first!
        let localURL = cacheURL.appendingPathComponent("firmware.bin")
        Logger.log("\(firmwareURL) -> \(localURL)")
        try? FileManager.default.removeItem(at: localURL)
        do {
            try FileManager.default.copyItem(at: firmwareURL, to: localURL)
        } catch {
            Logger.logError("\(error)")
            return nil
        }
        return localURL
    }

    private func setupStackView() {
        stackView.removeAllArrangedSubviews()
        let arrayViews = fotaTypeToggle.selectedSegmentIndex == 0 ? [fotaTypeToggle, pickFirmwareButton, firmwareLabel, startFOTAButton, progressLabel, errorLabel] : [fotaTypeToggle, pickLeftFOTAFirmwareButton, pickRightFOTAFirmwareButton, firmwareLabel, startFOTAButton, progressLabel, errorLabel]
        arrayViews.forEach { view in
            stackView.addArrangedSubview(view)
        }
    }

    private func startFOTA() {
        let alert = UIAlertController(title: "Update Firmware?", message: "Updating with the wrong firmware may damage the headset", preferredStyle: .alert)
        let startFOTAAction = UIAlertAction(title: "Update", style: .destructive) { [weak self] _ in self?.doStartFOTA() }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        alert.addAction(startFOTAAction)
        alert.addAction(cancelAction)
        present(alert, animated: true)
    }

    private func doStartFOTA() {
        Logger.log()
        guard
            let fotaSettings = self.createFotaSettings(),
            let deviceInteractor = self.deviceInteractor,
            deviceInteractor.isConnectedAndStarted
        else {
            errorLabel.text = "Not connected and started"
            return
        }
        beginFOTA()
        // TODO: mSDK5 CONSIDER removing scheduler, this is a concrete service concern
        let scheduler = ConcurrentDispatchQueueScheduler(queue: .init(label: "DeviceFOTAVC"))
        fotaDisposable?.dispose()
        fotaDisposable = deviceInteractor.firmwareUpdate
            .startUpdate(with: fotaSettings)
            .subscribeOn(scheduler)
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] progress in
                let progressValue = Int(progress.value * 100)
                if progressValue < 100 {
                    self?.progressLabel.text = "Updating: \(progressValue)%"
                } else {
                    self?.progressLabel.text = "Finalizing..."
                }
            }, onError: { [weak self] error in
                Logger.logError("\(error)")
                self?.endFOTA(error: error)
            }, onCompleted: { [weak self] in
                Logger.log("FOTA completed")
                self?.endFOTA(error: nil)
            })
        fotaDisposable?.disposed(by: disposeBag)
    }

    private func createFotaSettings() -> FotaSettings? {
        if fotaTypeToggle.selectedSegmentIndex == 1 {
            guard let leftFile = self.localLeftFirmwareURL,
                  let rightFile = self.localRightFirmwareURL
            else { errorLabel.text = "Files are empty"
                return nil
            }
            return FotaSettings(mode: .dual(letfPath: leftFile.path, rightPath: rightFile.path), batteryThreshold: 25)
        } else {
            guard let file = self.localFirmwareURL
            else { errorLabel.text = "File is empty"
                return nil
            }
            return FotaSettings(mode: .single(filePath: file.path), batteryThreshold: 25)
        }
    }

    private func beginFOTA() {
        progressLabel.text = nil
        errorLabel.text = nil
        activityIndicator.show(over: view)
        isModalInPresentation = true
        UIApplication.shared.isIdleTimerDisabled = true
    }

    private func endFOTA(error: Error?) {
        if let error = error {
            errorLabel.text = "\(error)"
        } else {
            progressLabel.text = "Completed"
        }
        disableButtons()
        activityIndicator.hide()
        // TODO: CONSIDER extracting stop
        deviceInteractor?.stop().subscribe().disposed(by: self.disposeBag)
        isModalInPresentation = false
        UIApplication.shared.isIdleTimerDisabled = false
    }

    @objc private func close() {
        dismiss(animated: true)
    }

    @objc private func setFOTAType() {
        firmwareURL = nil
        localFirmwareURL = nil
        localLeftFirmwareURL = nil
        localRightFirmwareURL = nil
        setupStackView()
        refreshUI()
    }

    private func refreshUI() {
        if fotaTypeToggle.selectedSegmentIndex == 0 {
            startFOTAButton.isEnabled = firmwareURL != nil
        } else {
            startFOTAButton.isEnabled = localLeftFirmwareURL != nil && localRightFirmwareURL != nil
        }
        firmwareLabel.text = firmwareURL?.lastPathComponent
    }

    private func disableButtons() {
        startFOTAButton.isEnabled = false
        pickFirmwareButton.isEnabled = false
    }
}
