//
// Created by Akos Polster on 07/05/2022
// Copyright (C) 2022 Bragi. All rights reserved.
//

import UIKit
import mSDK

/// Presents a button to initiate FOTA
class FOTAVC: FeatureVC {
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Firmware Update"
        contentView.addSubviewsForAutolayout(infoLabel, fotaButton)
        NSLayoutConstraint.activate([
            infoLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            infoLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            infoLabel.topAnchor.constraint(equalTo: contentView.topAnchor),
            fotaButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            fotaButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            fotaButton.topAnchor.constraint(equalTo: infoLabel.bottomAnchor, constant: 5),
            contentView.bottomAnchor.constraint(equalTo: fotaButton.bottomAnchor),
        ])
    }

    override func render() {
        super.render()
        if deviceInteractor?.isFeatureAvailable(.fota()) == true {
            infoLabel.text = nil
            fotaButton.isEnabled = true
        } else {
            infoLabel.text = "Feature not available"
            fotaButton.isEnabled = false
        }
    }

    // MARK: - Internal

    lazy var infoLabel = UILabel()
    lazy var fotaButton = BigButton(title: "Update Firmware") { [weak self] in
        self?.present(UINavigationController(rootViewController: FOTARunnerVC(deviceInteractor: self?.deviceInteractor)), animated: true)
    }
}
