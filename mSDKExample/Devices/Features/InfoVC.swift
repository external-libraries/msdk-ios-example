//
// Created by Akos Polster on 07/05/2022
// Copyright (C) 2022 Bragi. All rights reserved.
//

import UIKit
import mSDK
import RxSwift
import RxBragi

/// Presents device information
class InfoVC: FeatureVC {
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Device Information"
        contentView.addSubviewsForAutolayout(infoLabel, batteryLabel, observedBatteryLabel)
        NSLayoutConstraint.activate([
            infoLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            infoLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            infoLabel.topAnchor.constraint(equalTo: contentView.topAnchor),

            batteryLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            batteryLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            batteryLabel.topAnchor.constraint(equalTo: infoLabel.bottomAnchor, constant: 5),

            observedBatteryLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            observedBatteryLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            observedBatteryLabel.topAnchor.constraint(equalTo: batteryLabel.bottomAnchor),

            contentView.bottomAnchor.constraint(equalTo: observedBatteryLabel.bottomAnchor)
        ])
    }

    override func render() {
        super.render()
        guard deviceInteractor?.isConnectedAndStarted == true else {
            infoLabel.text = "Not connected"
            return
        }
        guard deviceInteractor?.isFeatureAvailable(.device) == true else {
            infoLabel.text = "Feature unavailable"
            return
        }
        renderDeviceInfo()
        renderBattery()
    }

    // MARK: - Internal

    private func renderDeviceInfo() {
        let device = deviceInteractor?.device
        let deviceID = device?.identifier ?? "n/a"
        var text = "ID: \(deviceID)"
        if let customName = device?.customName { text += "\nCustom name: \(customName)" }
        if let mfiName = device?.mfiDevice?.name { text += "\nMFI name: \(mfiName)" }
        if let bleName = device?.blePeripheral?.advertisedLocalName { text += "\nBLE advertised name: \(bleName)" }
        if let manufacturer = device?.manufacturer {
            text += "\nManufacturer: \(manufacturer)"
        } else if let manufacturer = device?.deviceConfig?.manufacturerDescription {
            text += "\nManufacturer: \(manufacturer)"
        }
        infoLabel.text = text
        deviceInfoDisposable?.dispose()
        deviceInfoDisposable = deviceInteractor?.api.device
            .getDeviceInfo()
            .subscribe(onSuccess: { [weak self] info in
                var text = "Type: \(info.type)"
                text += "\nGender: \(info.gender)"
                text += "\nSoftware version: \(info.sofwareVersion)"
                text += "\nFirmware version: \(info.firmwareVersion)"
                text += "\nFirmware revision: \(info.revision)"
                text += "\nHardware version: \(info.hardwareVersion)"
                if let serial = info.serialNumber { text += "\nSerial number: \(serial)" }
                if let model = info.model { text += "\nModel: \(model)" }
                if let chip = info.chip { text += "\nChip: \(chip)" }
                self?.infoLabel.text = (self?.infoLabel.text ?? "") + "\n" + text
                self?.infoLabel.sizeToFit()
            }, onError: { [weak self] error in
                Logger.logError("\(error)")
                self?.infoLabel.text = (self?.infoLabel.text ?? "") + "\n" + "\(error)"
                self?.infoLabel.sizeToFit()
            })
        deviceInfoDisposable?.disposed(by: disposeBag)
    }

    private func renderBattery() {
        batteryDisposable?.dispose()
        batteryDisposable = deviceInteractor?.api.device
            .getBatteryStatus()
            .subscribe(onSuccess: { [weak self] status in
                self?.batteryLabel.text = "Battery (spot): \(status)"
            }, onError: { [weak self] error in
                self?.batteryLabel.text = "Battery (spot): \(error)"
            })
        batteryDisposable?.disposed(by: disposeBag)
        batteryObservationDisposable?.dispose()
        batteryObservationDisposable = deviceInteractor?.api.device
            .observeBatteryStatus(interval: .seconds(30))
            .subscribe(onNext: { [weak self] status in
                self?.observedBatteryLabel.text = "Battery (observed): \(status)"
            }, onError: { [weak self] error in
                self?.observedBatteryLabel.text = "Battery (observed): \(error)"
            })
        batteryObservationDisposable?.disposed(by: disposeBag)
    }

    private lazy var infoLabel = MultiLineLabel()
    private lazy var batteryLabel = MultiLineLabel()
    private lazy var observedBatteryLabel = MultiLineLabel(text: "Battery (observed): Pending")
    private lazy var disposeBag = DisposeBag()
    private var deviceInfoDisposable: Disposable?
    private var batteryDisposable: Disposable?
    private var batteryObservationDisposable: Disposable?
}
