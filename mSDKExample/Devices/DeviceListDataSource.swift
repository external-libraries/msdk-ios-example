//
// Created by Akos Polster on 23/04/2022.
// Copyright (C) 2022 Bragi. All rights reserved.
//

import UIKit
import mSDK
import RxBragi
import RxSwift

/// Provides cells for the device list
class DeviceListDataSource: NSObject, UITableViewDataSource {
    init(tableView: UITableView) {
        super.init()
        self.tableView = tableView
        tableView.register(DeviceCell.self, forCellReuseIdentifier: Self.deviceCellID)
    }

    /// Restart discovering devices
    func discover() {
        devicesSet.removeAll()
        tableView?.reloadData()
        discoverDevicesDisposable?.dispose()
        let discoverDevicesDisposable = Bragi.shared
            .discoverDevices(DeviceConfig.all)
            .do(onNext: { [weak self] device in
                Logger.log("\(device.longName) \(device.identifier)")
                self?.devicesSet.insert(device)
                self?.tableView?.reloadData()
            }, onError: { error in
                Logger.log("❌ \(error)")
            })
            .subscribe()
        discoverDevicesDisposable.disposed(by: disposeBag)
        self.discoverDevicesDisposable = discoverDevicesDisposable
    }

    func device(at indexPath: IndexPath) -> Device? {
        devices[safe: indexPath.row]
    }

    // MARK: - UITableViewDataSource

    func numberOfSections(in tableView: UITableView) -> Int {
        1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        devices.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Self.deviceCellID, for: indexPath)
        guard let deviceCell = cell as? DeviceCell else { return cell }
        deviceCell.device = devices[safe: indexPath.row]
        return deviceCell
    }

    // MARK: - Internal

    private var devices: [Device] { Array(devicesSet) }
    private var devicesSet = Set<Device>()
    private weak var tableView: UITableView?
    private lazy var disposeBag = DisposeBag()
    private var discoverDevicesDisposable: Disposable?
    private static let deviceCellID = "deviceCell"
}
