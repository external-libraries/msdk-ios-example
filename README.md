# mSDK iOS Example

Example iOS application demonstrating how to use the binary Bragi mSDK distribution.

## Important Considerations

- Contact Bragi to get access to the CocoaPod repository at [https://gitlab.com/external-libraries](https://gitlab.com/external-libraries).

- Bragi mSDK is only available for iOS, version 13.0 or later. Therefore, add ```platfom :ios, '13.0'``` to the Podfile

- Bragi mSDK is only built for distribution, therefore other dependencies should be built for distribution as well. See the ```post_install`` section in the example Podfile

- Due to lack of simulator support in some 3rd party frameworks used by mSDK, we only provide _arm64_ frameworks

- Due to lack of Bitcode support in some 3rd party frameworks used by mSDK, your application should also disable Bitcode under _Build Settings_ in Xcode.

## Building The Example

- Install [CocoaPods](https://cocoapods.org)

- Clone this repository

- Run ```pod install``` in the root folder

- Open the _mSDKExample.xcworkspace_ generated above

- Build and run the example targeting an actual iOS device
